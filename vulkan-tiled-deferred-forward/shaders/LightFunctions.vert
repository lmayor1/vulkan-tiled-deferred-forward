#extension GL_ARB_separate_shader_objects : enable

#ifndef LIGHT_FUNCTIONS
#define LIGHT_FUNCTIONS

void PhongBlinn( GeometryData _surfaceData, PointLightData _light, inout vec3 _lit ) {

	vec3  lightDirection = _light.PositionRadius.xyz - _surfaceData.ViewSpacePosition.xyz;
	float lightDistance  = length( lightDirection );

	if( lightDistance < _light.PositionRadius.w ) {
		float attenuation = clamp(1.0f - lightDistance / _light.PositionRadius.w, 0.0, 1.0);
		lightDirection *= (1.0/lightDistance);

		vec3 diffuse  = vec3(0.0, 0.0, 0.0);
		vec3 specular = vec3(0.0, 0.0, 0.0);
		float angle   = dot(_surfaceData.ViewSpaceNormal.xyz, lightDirection);

		if(angle > 0.0) {
			vec3 reflNormal   = reflect(lightDirection, _surfaceData.ViewSpaceNormal.xyz);
			float viewAngle   = max(0.0, dot(reflNormal, normalize(_surfaceData.ViewSpacePosition.xyz)));
			float specAmount  = pow(viewAngle, 25.0);
			vec3 contribution = vec3(attenuation) * _light.Colour.rgb;

			diffuse  += contribution * angle;
			specular += contribution * specAmount;
		}

		bool lightColOnly = false;
		if(lightColOnly) {
			_lit += (diffuse + 5.0 * specular);
		} else {
			_lit += _surfaceData.Diffuse.rgb * (diffuse + 5.0 * specular);
		}
	}
}

#endif