#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_GOOGLE_include_directive : enable

#include "Constants.h"
#include "DataStructures.vert"
#include "PerFrame.vert"

// Textures (assumed to have diff/spec/normal textures supplied)
layout(binding = 3) uniform sampler2D diffuseTex;
layout(binding = 4) uniform sampler2D specularTex;
layout(binding = 5) uniform sampler2D normalTex;	// (ignoring texture normal maps for now)

// vert
layout(location = 0) in vec3 fragWorldSpaceNormal;
layout(location = 1) in vec3 fragTangent;
layout(location = 2) in vec2 fragTexCoord;

// frag (GBuffer)
layout(location = 0) out vec4 outDiffuse;
layout(location = 1) out vec4 outSpecular;
layout(location = 2) out vec4 outNormal;

void main() {
	outDiffuse  = vec4(0.0, 1.0, 0.0, 1.0);
	outSpecular = vec4(0.0, 1.0, 0.0, 1.0);
	outNormal   = vec4(0.0, 1.0, 0.0, 1.0);
}