#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_GOOGLE_include_directive : enable

#include "Constants.h"
#include "DataStructures.vert"
#include "PerFrame.vert"
#include "LightFunctions.vert"

// Textures (assumed to have diff/spec/normal textures supplied)
layout(binding = 3) uniform sampler2D diffuseTex;
layout(binding = 4) uniform sampler2D specularTex;
layout(binding = 5) uniform sampler2D normalTex;

// vert
layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec3 fragWorldSpacePos;
layout(location = 2) in vec3 fragWorldSpaceNormal;
layout(location = 3) in vec3 fragTangent;
layout(location = 4) in vec2 fragTexCoord;

// frag
layout(location = 0) out vec4 outColour;

void main() {
	outColour = texture(diffuseTex, fragTexCoord);
}

/*
	// fill surface information
	GeometryData surface;
	surface.WorldSpacePos     = fragWorldSpacePos;
	surface.WorldSpaceNormal  = fragWorldSpaceNormal;
	surface.Diffuse           = texture(diffuseTex,   fragTexCoord).rgb;

	PointLightData currentLight;
	vec3 lit = vec3(0);
	int num = sMultiLights.light.length();
	for( uint light = 0; light < num; ++light ){
		currentLight.PositionRadius = sMultiLights.light[light].PositionRadius;
		currentLight.Colour         = sMultiLights.light[light].Colour;
		PhongBlinn( surface, currentLight, lit );
	}
	outColour = vec4(colour.rgb, 1.0);
*/