#extension GL_ARB_separate_shader_objects : enable

#ifndef DATA_STRUCTURES
#define DATA_STRUCTURES

struct PointLightData {
	vec4 PositionRadius;
	vec4 Colour;
};

struct GeometryData {
	vec3 ViewSpacePosition;
	vec3 ViewSpaceNormal;
	vec4 Diffuse;
};

#endif