#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_GOOGLE_include_directive : enable

#include "Constants.h"
#include "DataStructures.vert"
#include "PerFrame.vert"

// input
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inColor;
layout(location = 3) in vec3 inTangent;
layout(location = 4) in vec2 inTexCoord;

// vert
layout(location = 0) out vec3 fragWorldSpaceNormal;
layout(location = 1) out vec3 fragTangent;
layout(location = 2) out vec2 fragTexCoord;

out gl_PerVertex {
	vec4 gl_Position;
};

void main() {
	gl_Position          = uCameraMatrices.proj * uCameraMatrices.view * pC.model * vec4(inPosition, 1.0);	
	fragWorldSpaceNormal = (pC.model * vec4(inNormal, 0.0)).xyz;
	fragTangent			 = inTangent;
	fragTexCoord         = inTexCoord;
}