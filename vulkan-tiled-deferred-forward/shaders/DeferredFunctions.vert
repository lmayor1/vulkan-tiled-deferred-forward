#extension GL_ARB_separate_shader_objects : enable

#ifndef DEFERRED_FUNCTIONS
#define DEFERRED_FUNCTIONS

vec3 PositionFromDepth( vec2 _uv, float _depth ) {

	vec2 vpRay = vec2( _uv.x / uMVP.proj[0].x, _uv.y / uMVP.proj[1].y );
	return vec3( vpRay.xy * _depth, _depth );
}

GeometryData GetDataFromGBuffer( ivec2 _uv ) {

	GeometryData __out;

	vec4  diffuseSample = texture(GDiffuse, _uv.xy);
	vec3  normalSample  = texture(GNormal,  _uv.xy).xyz;
	float depthSample   = texture(GDepth,   _uv.xy).x;

	vec2 texDim = textureSize(GDiffuse, 0);
	vec2 offset = vec2(2.0,-2.0) / texDim.xy;
	vec2 vpPos  = (vec2(_uv.xy) + 0.5) * offset.xy + vec2(-1.0,1.0);

	__out.ViewSpacePosition = PositionFromDepth(vpPos, depthSample);
	__out.ViewSpaceNormal   = normalSample;
	__out.Diffuse           = diffuseSample;

	return __out;
}

#endif