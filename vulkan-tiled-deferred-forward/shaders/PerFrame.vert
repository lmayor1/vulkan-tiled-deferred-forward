#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform CameraMatrices {
	mat4 view;
	mat4 proj;
} uCameraMatrices;

layout(binding = 1) uniform AppData {
	vec4 CameraPos;
} uAppData;

layout(binding = 2) buffer MultiLights {
	PointLightData light[];
} sMultiLights;

layout(push_constant) uniform PushConstantsObject {
	mat4 model;
} pC;