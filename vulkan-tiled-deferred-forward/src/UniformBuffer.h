#pragma once

/*	Author: Louis Mayor
	Data: 10/09/18

	Vulkan Uniform Buffer
*/

#include <cassert>
#include "VulkanHelpers.h"

template<typename T>
class UniformBuffer {
public:
	UniformBuffer( ) = default;
	~UniformBuffer( ) {
	}

	UniformBuffer( VkPhysicalDevice const _physicalDevice, VkDevice const _device, const bool&& _dynamicBuffer = false, const int&& _totalBuffers = 1 ) {

		assert( _totalBuffers >= 1 );
		mDevice          = _device;
		mPhysicalDevice  = _physicalDevice;
		mAmountOfBuffers = _totalBuffers;
		mBufferSize      = sizeof( T );
		isDestroyed      = false;
		mUniformBuffers.resize( _totalBuffers );
		mUniformBuffersMemory.resize( _totalBuffers );
		SetTotalInstances( );
	}

	void Initialise( VkPhysicalDevice const _physicalDevice, VkDevice const _device, const int _totalBuffers = 1, const bool&& _dynamicBuffer = false ) {

		assert( _totalBuffers >= 1 );
		mDevice          = _device;
		mPhysicalDevice  = _physicalDevice;
		mAmountOfBuffers = _totalBuffers;
		mBufferSize      = sizeof( T );
		isDestroyed      = false;
		mUniformBuffers.resize( _totalBuffers );
		mUniformBuffersMemory.resize( _totalBuffers );
		SetTotalInstances( );
	}

	void CreateDescriptorSetLayout( const VkShaderStageFlagBits&& _stageFlag, const uint32_t&& _dstBinding = -1 ) {

		mDescriptorSetLayout = createDescriptorSetLayout( VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, _stageFlag, _dstBinding );
	}

	void CreateDescriptorSet( const size_t _bufferIndex, const VkStructureType&& _structType,
							  const VkDescriptorSet& _descSet, const uint32_t _dstBinding ) {

		mDescriptorSet = createDescriptorSet( _structType, _descSet, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, &GetBufferInfo( _bufferIndex ), nullptr, _dstBinding );
	}

	void UpdateDescriptorSet( ) {

		vkUpdateDescriptorSets( mDevice, 1, &GetDescriptorSet( ), 0, nullptr );
	}

	void AllocateBuffer( const size_t _index, const bool _automaticFlushing = true ) {

		VkMemoryPropertyFlags __memProps = ( _automaticFlushing ) ?
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT : VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;

		createBuffer( mPhysicalDevice, mDevice,
					  GetBufferSize( ), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
					  __memProps, GetBuffer( _index ), GetBufferMemory( _index ) );
	}

	void Map( const int _currentBuffer = 0 ) {

		void* data;
		vkMapMemory( mDevice, GetBufferMemory( _currentBuffer ), 0, static_cast<size_t>( GetBufferSize( ) ), 0, &data );
		memcpy( data, &GetInstance( 0 ), static_cast<size_t>( GetBufferSize( ) ) );
		vkUnmapMemory( mDevice, GetBufferMemory( _currentBuffer ) );
	}

	void Destroy( ) {

		if( !isDestroyed ) {
			for( size_t i = 0; i < mAmountOfBuffers; i++ ) {
				if( mUniformBuffers[i] != 0 && mUniformBuffersMemory[i] != 0 ) {
					vkDestroyBuffer( mDevice, mUniformBuffers[i], nullptr );
					vkFreeMemory( mDevice, mUniformBuffersMemory[i], nullptr );
					mUniformBuffers[i] = 0;
					mUniformBuffersMemory[i] = 0;
				}
			}
			isDestroyed = !isDestroyed;
		}		
	}

	VkDescriptorBufferInfo& GetBufferInfo( const size_t _index = 0 ) {

		mBufferInfo.buffer = GetBuffer( _index );
		mBufferInfo.offset = 0;
		mBufferInfo.range  = mBufferSize * mInstances.size( );

		return mBufferInfo;
	}

	VkBuffer& GetBuffer( const size_t _index = 0 ) {
		return mUniformBuffers[_index];
	}

	VkDeviceMemory& GetBufferMemory( const size_t _index = 0 ) {
		return mUniformBuffersMemory[_index];
	}

	inline VkDeviceSize GetBufferSize( ) const {
		return mBufferSize * mInstances.size( );
	}

	inline VkDescriptorSetLayoutBinding& GetDescriptorLayout( ) {
		return mDescriptorSetLayout;
	}

	inline VkWriteDescriptorSet& GetDescriptorSet( ) {
		return mDescriptorSet;
	}

	void SetTotalInstances( const size_t _index = 1 ) {
		mInstances.clear( );
		mInstances.resize( _index );
	}

	T& GetInstance( const size_t _index = 0 ) {
		return mInstances[_index];
	}
	
	std::vector<T>& GetInstances( ) {
		return mInstances;
	}

private:	
	UniformBuffer( const UniformBuffer& _other )  = delete;
	void operator=( const UniformBuffer& _other ) = delete;

	size_t mAmountOfBuffers;
	std::vector<VkBuffer> mUniformBuffers;
	std::vector<VkDeviceMemory> mUniformBuffersMemory;
	VkBufferUsageFlagBits mBufferUsage;
	VkDeviceSize mBufferSize;
	VkDevice mDevice;
	VkPhysicalDevice mPhysicalDevice;
	VkDescriptorBufferInfo mBufferInfo;
	VkDescriptorSetLayoutBinding mDescriptorSetLayout;
	VkWriteDescriptorSet mDescriptorSet;
	std::vector<T> mInstances;

	bool isDestroyed = false;
};