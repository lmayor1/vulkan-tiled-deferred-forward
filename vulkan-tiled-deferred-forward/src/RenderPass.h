#pragma once

#include "VulkanHelpers.h"

class RenderPass {
public:
	RenderPass( ) = default;
	~RenderPass( ) {

	}

	void CreateRenderPass(	VkAttachmentReference* const _colourAttachments, const size_t _ColourAttachmentCount,
							VkAttachmentReference* const _depthAttachment,
							VkAttachmentReference* const _resolveAttachments,
						    const std::vector<VkAttachmentDescription>& _attachments,
						    const bool _compute, VkDevice const _device ) {

		// standard
		mSubpassDesc.pipelineBindPoint       = ( _compute ) ? VK_PIPELINE_BIND_POINT_COMPUTE : VK_PIPELINE_BIND_POINT_GRAPHICS;
		mSubpassDesc.colorAttachmentCount    = _ColourAttachmentCount;
		mSubpassDesc.pColorAttachments       = _colourAttachments;
		mSubpassDesc.pDepthStencilAttachment = _depthAttachment;
		mSubpassDesc.pResolveAttachments     = _resolveAttachments;
		// standard (TODO: would change if multiple subpasses required, i.e. deferred)
		mSubpassDepends.srcSubpass           = VK_SUBPASS_EXTERNAL;
		mSubpassDepends.dstSubpass           = 0;
		mSubpassDepends.srcStageMask         = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		mSubpassDepends.srcAccessMask        = 0;
		mSubpassDepends.dstStageMask         = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		mSubpassDepends.dstAccessMask        = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		// Same as above
		mPassInfo.sType                      = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		mPassInfo.attachmentCount            = _attachments.size( );
		mPassInfo.pAttachments               = _attachments.data( );
		mPassInfo.subpassCount               = 1;
		mPassInfo.pSubpasses                 = &mSubpassDesc;
		mPassInfo.dependencyCount            = 1;
		mPassInfo.pDependencies              = &mSubpassDepends;

		if( vkCreateRenderPass( _device, &mPassInfo, nullptr, &mRenderpass ) != VK_SUCCESS ) {
			throw std::runtime_error( "failed to create render pass!" );
		}
	}

	bool Destroy( VkDevice const _device ) {
		if( mRenderpass != 0 ) {
			vkDestroyRenderPass( _device, mRenderpass, nullptr );
			mRenderpass = 0;
			return true;
		}
		return false;
	}

	VkRenderPass GetRenderPass( ) const {
		return mRenderpass;
	}

private:
	VkSubpassDescription	mSubpassDesc    = {};
	VkSubpassDependency		mSubpassDepends = {};
	VkRenderPassCreateInfo	mPassInfo       = {};
	VkRenderPass mRenderpass;
};