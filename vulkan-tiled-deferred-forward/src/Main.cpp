#include <memory>
#include <string>

#include "VulkanApp.h"

#if defined(_WIN32) || defined(_WIN64)

void EnableCMD( int _enable ) {
	HWND hWnd = GetConsoleWindow( );
	ShowWindow( hWnd, _enable );
}
#endif

#ifndef NDEBUG
bool  enableMouseControl = false;
#else
bool  enableMouseControl = true;
EnableCMD( false );
#endif

void MouseControlCallback( GLFWwindow* window, double _x, double _y );
bool Initialise( );
bool Load( );
bool Loop( );
bool Clean( );

Logger gLogger;

bool initMouse = true;
double __x;
double __y;

std::unique_ptr<vk>		app;
std::unique_ptr<Camera> mainCamera;

GLFWwindow* windowHandle;

float BufferResolution[] = { 800.0f, 600.0f };
std::string windowTitle  = "Vulkan Renderer";
std::string appTitle     = "Tiled Deferred and Forward";
std::string windowName;

int main( ) {

	if( !Initialise( ) ) {
		Clean( );
		return -1;
	}

	if( !Load( ) ) {
		Clean( );
		return -1;
	}

	if( !Loop() ) {
		Clean( );
	}

	return 0;
}

bool Initialise( ) {

	glfwInit( );

	glfwWindowHint( GLFW_CLIENT_API, GLFW_NO_API );
	glfwWindowHint( GLFW_RESIZABLE, GLFW_TRUE );

	windowHandle = glfwCreateWindow( static_cast<int>( BufferResolution[0] ),
									 static_cast<int>( BufferResolution[1] ),
									 windowName.c_str( ), nullptr, nullptr );
	if( windowHandle == nullptr ) {
		std::cerr << "Failed to create GLFW window" << std::endl;
		glfwTerminate( );
		return false;
	}

	app        = std::make_unique<vk>( );
	mainCamera = std::make_unique<Camera>( );

	glfwSetWindowPos( windowHandle, 0, 30 );

#ifndef NDEBUG
	HANDLE cmdHandle = GetStdHandle( STD_OUTPUT_HANDLE );
	int x = glfwGetVideoMode( glfwGetPrimaryMonitor( ) )->width;
	SetWindowPos( GetConsoleWindow( ), 0, x - 1024, 0, 0, 0, SWP_NOSIZE | SWP_NOZORDER );
	gLogger.Create( cmdHandle );
#else
	HANDLE cmdHandle = nullptr;
	glfwSetInputMode( windowHandle, GLFW_CURSOR, GLFW_CURSOR_DISABLED );
#endif

	InitialiseInput( );
	glfwSetKeyCallback( windowHandle, KeyEvent );
	glfwSetCursorPosCallback( windowHandle, enableMouseControl ? MouseControlCallback : nullptr );
	glfwSetInputMode( windowHandle, GLFW_CURSOR, enableMouseControl ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL );

	if( !app->Initialise( windowHandle ) ) {
		return false;
	}
	return true;
}

bool Load( ) {

	auto dims = app->GetViewportDimensions( );
	mainCamera->SetProjectionMatrix( dims.x, dims.y, 70.0f, 0.1f, 1000.0f );
	mainCamera->SetPosition( glm::vec3( 1.f, 1.5f, 2.f ) );
	mainCamera->SetRotation( glm::vec3( 0.f, -90.f, 0.f ) );

	app->SetCamera( mainCamera.get( ) );

	glfwSetWindowTitle( windowHandle, "Loading" );

	std::unique_ptr<Model> sphere = std::make_unique<Model>( );
	if( !sphere->Load( "assets/shapes", "sphere.obj" ) ) {
		return false;
	}

	sphere->SetScale( 0.05f );
	sphere->SetPosition( glm::vec3( 1.f, 1.f, 1.f ) );

	app->AddModelToRenderList( std::move( sphere ) );

	std::unique_ptr<Model> player = std::make_unique<Model>( );
	if( !player->Load( "assets/nanosuit", "nanosuit.obj", "assets/nanosuit" ) ) {
		return false;
	}

	player->SetScale( 0.05f );
	player->SetPosition( glm::vec3( 1.f, 1.f, 1.f ) );

	app->AddModelToRenderList( std::move( player ) );

	return true;
}

bool Loop( ) {

	float init_time = 0;
	while( !glfwWindowShouldClose( windowHandle ) ) {

		float total_time = static_cast<float>( glfwGetTime( ) );
		float delta = total_time - init_time;
		init_time = total_time;

		windowName.clear( );
		windowName = windowTitle + ": " + appTitle + " ||";
		windowName += " Delta " + std::to_string( delta );
		windowName += " FPS " + std::to_string( 1 / delta );
		windowName += " Timer " + std::to_string( total_time );

		glfwSetWindowTitle( windowHandle, windowName.c_str( ) );
		glfwPollEvents( );

		if( !app->ProcessInput( ) ) {
			return false;
		}

		if( KeyHit( eKeyCodes::KeyT ) ) {
			enableMouseControl = !enableMouseControl;
			glfwSetInputMode( windowHandle, GLFW_CURSOR, enableMouseControl ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL );
			glfwSetCursorPosCallback( windowHandle, enableMouseControl ? MouseControlCallback : nullptr );
			initMouse = enableMouseControl;
		}

		if( KeyHit( eKeyCodes::KeyU ) ) {
//			app->AddModelToRenderList( std::move( player ) );
		}

		app->Update( delta );
		app->Render( delta );
	}

	return true;
}

bool Clean( ) {
	
	app->Cleanup( );

	glfwDestroyWindow( windowHandle );
	glfwTerminate( );
	
	return true;
}

void MouseControlCallback( GLFWwindow* window, double _x, double _y ) {

	if( initMouse ) {
		__x = _x;
		__y = _y;
		initMouse = false;
	}	

	float __xOffset = static_cast<float>( _x ) - static_cast<float> ( __x );
	float __yOffset = static_cast<float>( _y ) - static_cast<float> ( __y );

	__x = _x;
	__y = _y;

	mainCamera->MouseControl( __xOffset, __yOffset );
}