#pragma once

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>

#include "Light.h"

namespace UBO {

	struct ModelMatrix {
		glm::mat4 model;
	};

	struct CameraMatrces {
		glm::mat4 view;
		glm::mat4 proj;
	};

	struct LightData {
		Light pointLight;
	};

	struct AppData {
		glm::vec4 CameraPosition;
	};
}