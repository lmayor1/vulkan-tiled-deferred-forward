#pragma once

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/hash.hpp>

#include <unordered_map>
#include <array>

#include "VulkanHelpers.h"

struct Vertex {

	glm::vec3 pos;
	glm::vec3 normal;
	glm::vec3 color;
	glm::vec3 tangent;
	glm::vec2 texCoord;

	static VkVertexInputBindingDescription getBindingDescription( ) {
		VkVertexInputBindingDescription bindingDescription = {};
		bindingDescription.binding   = 0;
		bindingDescription.stride    = sizeof( Vertex );
		bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		return bindingDescription;
	}

	static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions( ) {

		std::vector<VkVertexInputAttributeDescription> attributeDescriptions;
		attributeDescriptions.resize( 5 );

		attributeDescriptions[0].binding  = 0;
		attributeDescriptions[0].location = 0;
		attributeDescriptions[0].format   = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[0].offset   = offsetof( Vertex, pos );

		attributeDescriptions[1].binding  = 0;
		attributeDescriptions[1].location = 1;
		attributeDescriptions[1].format   = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[1].offset   = offsetof( Vertex, normal );

		attributeDescriptions[2].binding  = 0;
		attributeDescriptions[2].location = 2;
		attributeDescriptions[2].format   = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[2].offset   = offsetof( Vertex, color );

		attributeDescriptions[3].binding  = 0;
		attributeDescriptions[3].location = 3;
		attributeDescriptions[3].format   = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[3].offset   = offsetof( Vertex, tangent );

		attributeDescriptions[4].binding  = 0;
		attributeDescriptions[4].location = 4;
		attributeDescriptions[4].format   = VK_FORMAT_R32G32_SFLOAT;
		attributeDescriptions[4].offset   = offsetof( Vertex, texCoord );

		return attributeDescriptions;
	}

	bool operator==( const Vertex& other ) const {
		return ( pos == other.pos && normal == other.normal && color == other.color && texCoord == other.texCoord );
	}
};

namespace std {
	template<> struct hash<Vertex> {
		size_t operator()( Vertex const& vertex ) const {
			return ( ( hash<glm::vec3>( )( vertex.pos ) ^ ( hash<glm::vec3>( )( vertex.color ) << 1 ) ) >> 1 ) ^ ( hash<glm::vec2>( )( vertex.texCoord ) << 1 );
		}
	};
}