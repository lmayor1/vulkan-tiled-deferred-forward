#pragma once

/*	Author: Louis Mayor
	Data: 10/09/18

	Vulkan Texture Image
*/

#include "VulkanHelpers.h"

// macro 'STB_IMAGE_IMPLEMENTATION' defined in VulkanApp.cpp
#include <stb-master/stb_image.h>

class TextureImage {
public:
	TextureImage( ) = default;
	~TextureImage( ) {
	}

	void Create( VkPhysicalDevice _physicalDevice, VkDevice _device, 
				 VkCommandPool _commandPool, VkQueue _queue,
				 const std::string _textureDirectory, const std::string _textureName ) {

		mDevice = _device;
		createTextureImage( _physicalDevice, _commandPool, _queue, _textureDirectory, _textureName );
		createTextureImageView( );
	}

	void CreateDescriptorSetLayout( const VkShaderStageFlagBits&& _stageFlag, const uint32_t&& _dstBinding = -1 ) {

		mDescriptorSetLayout = createDescriptorSetLayout( VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, _stageFlag, _dstBinding );
	}

	void CreateDescriptorSet( const size_t _bufferIndex, const VkStructureType&& _structType,
							  const VkDescriptorSet& _descSet, const uint32_t _dstBinding,
							  const VkSampler& _sampler ) {

		mDescriptorSet = createDescriptorSet( _structType, _descSet, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, nullptr, &GetImageInfo( _sampler ), _dstBinding );
	}

	void UpdateDescriptorSet( ) {

		vkUpdateDescriptorSets( mDevice, 1, &GetDescriptorSet( ), 0, nullptr );
	}

	void Destroy( ) {

		if( !isDestroyed ) {
			if( mDevice != nullptr && mTextureImageView != 0 && mTextureImage != 0 && mTextureImageMemory != 0 ) {
				vkDestroyImageView( mDevice, mTextureImageView, nullptr );
				vkDestroyImage( mDevice, mTextureImage, nullptr );
				vkFreeMemory( mDevice, mTextureImageMemory, nullptr );
			}
			isDestroyed = !isDestroyed;
		}
	}

	void SetImageView( VkImageView& _imageView ) {
		mTextureImageView = _imageView;
	}

	VkImageView& GetImageView( ) {
		return mTextureImageView;
	}

	uint32_t GetMipLevel( ) const {
		return mMipLevels;
	}

	VkDescriptorImageInfo& GetImageInfo( const VkSampler& _sampler ) {

		mImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		mImageInfo.imageView   = GetImageView( );
		mImageInfo.sampler     = _sampler;

		return mImageInfo;
	}

	inline VkDescriptorSetLayoutBinding& GetDescriptorLayout( ) {
		return mDescriptorSetLayout;
	}

	inline VkWriteDescriptorSet& GetDescriptorSet( ) {
		return mDescriptorSet;
	}

private:
	void createTextureImage( VkPhysicalDevice _physicalDevice, VkCommandPool _commandPool, VkQueue _queue, const std::string _textureDirectory, const std::string _textureName ) {

		int texWidth, texHeight, texChannels;
		std::string image = _textureDirectory + '/' + _textureName;
		stbi_uc* pixels = stbi_load( image.c_str( ), &texWidth, &texHeight, &texChannels, STBI_rgb_alpha );
		VkDeviceSize imageSize = texWidth * texHeight * 4;
		mMipLevels = static_cast<uint32_t>( std::floor( std::log2( std::max( texWidth, texHeight ) ) ) ) + 1;

		if( !pixels ) {
			throw std::runtime_error( "failed to load texture image!" );
		}

		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;

		createBuffer( _physicalDevice, mDevice, imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
					  VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
					  stagingBuffer, stagingBufferMemory );

		void* data;
		vkMapMemory( mDevice, stagingBufferMemory, 0, imageSize, 0, &data );
		memcpy( data, pixels, static_cast<size_t>( imageSize ) );
		vkUnmapMemory( mDevice, stagingBufferMemory );

		stbi_image_free( pixels );

		createImage( _physicalDevice, mDevice,
					 texWidth, texHeight, mMipLevels, VK_SAMPLE_COUNT_1_BIT,
					 VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL,
					 VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
					 mTextureImage, mTextureImageMemory );

		transitionImageLayout( mDevice, _commandPool, _queue, mTextureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, mMipLevels );
		copyBufferToImage( _commandPool, _queue, stagingBuffer, mTextureImage, static_cast<uint32_t>( texWidth ), static_cast<uint32_t>( texHeight ) );

		vkDestroyBuffer( mDevice, stagingBuffer, nullptr );
		vkFreeMemory( mDevice, stagingBufferMemory, nullptr );

		generateMipmaps( _physicalDevice, _commandPool, _queue, mTextureImage, VK_FORMAT_R8G8B8A8_UNORM, texWidth, texHeight, mMipLevels );
	}

	void copyBufferToImage( VkCommandPool _commandPool, VkQueue _queue, VkBuffer buffer, VkImage image, uint32_t width, uint32_t height ) {

		VkCommandBuffer commandBuffer = beginSingleTimeCommands( mDevice, _commandPool );

		VkBufferImageCopy region = {};
		region.bufferOffset = 0;
		region.bufferRowLength = 0;
		region.bufferImageHeight = 0;
		region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		region.imageSubresource.mipLevel = 0;
		region.imageSubresource.baseArrayLayer = 0;
		region.imageSubresource.layerCount = 1;
		region.imageOffset = { 0, 0, 0 };
		region.imageExtent = {
			width,
			height,
			1
		};

		vkCmdCopyBufferToImage( commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region );

		endSingleTimeCommands( mDevice, _commandPool, commandBuffer, _queue );
	}

	void generateMipmaps( VkPhysicalDevice _physicalDevice, VkCommandPool _commandPool, VkQueue _queue, VkImage image, VkFormat imageFormat, int32_t texWidth, int32_t texHeight, uint32_t mipLevels ) {

		// Check if image format supports linear blitting
		VkFormatProperties formatProperties;
		vkGetPhysicalDeviceFormatProperties( _physicalDevice, imageFormat, &formatProperties );

		if( !( formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT ) ) {
			throw std::runtime_error( "texture image format does not support linear blitting!" );
		}

		VkCommandBuffer commandBuffer = beginSingleTimeCommands( mDevice, _commandPool );

		VkImageMemoryBarrier barrier = {};
		barrier.sType                           = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.image                           = image;
		barrier.srcQueueFamilyIndex             = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex             = VK_QUEUE_FAMILY_IGNORED;
		barrier.subresourceRange.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
		barrier.subresourceRange.baseArrayLayer = 0;
		barrier.subresourceRange.layerCount     = 1;
		barrier.subresourceRange.levelCount     = 1;

		int32_t mipWidth = texWidth;
		int32_t mipHeight = texHeight;

		for( uint32_t i = 1; i < mipLevels; i++ ) {
			barrier.subresourceRange.baseMipLevel = i - 1;
			barrier.oldLayout                     = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			barrier.newLayout                     = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
			barrier.srcAccessMask                 = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask                 = VK_ACCESS_TRANSFER_READ_BIT;

			vkCmdPipelineBarrier( commandBuffer,
								  VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
								  0, nullptr,
								  0, nullptr,
								  1, &barrier );

			VkImageBlit blit = {};
			blit.srcOffsets[0]                 = { 0, 0, 0 };
			blit.srcOffsets[1]                 = { mipWidth, mipHeight, 1 };
			blit.srcSubresource.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
			blit.srcSubresource.mipLevel       = i - 1;
			blit.srcSubresource.baseArrayLayer = 0;
			blit.srcSubresource.layerCount     = 1;
			blit.dstOffsets[0]                 = { 0, 0, 0 };
			blit.dstOffsets[1]                 = { mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1 };
			blit.dstSubresource.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
			blit.dstSubresource.mipLevel       = i;
			blit.dstSubresource.baseArrayLayer = 0;
			blit.dstSubresource.layerCount     = 1;

			vkCmdBlitImage( commandBuffer,
							image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
							image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
							1, &blit,
							VK_FILTER_LINEAR );

			barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
			barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

			vkCmdPipelineBarrier( commandBuffer,
								  VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
								  0, nullptr,
								  0, nullptr,
								  1, &barrier );

			if( mipWidth  > 1 ) mipWidth /= 2;
			if( mipHeight > 1 ) mipHeight /= 2;
		}

		barrier.subresourceRange.baseMipLevel = mipLevels - 1;
		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		vkCmdPipelineBarrier( commandBuffer,
							  VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
							  0, nullptr,
							  0, nullptr,
							  1, &barrier );

		endSingleTimeCommands( mDevice, _commandPool, commandBuffer, _queue );
	}

	void createTextureImageView( ) {
		mTextureImageView = createImageView( mDevice, mTextureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT, mMipLevels );
	}

private:
	VkImage mTextureImage;
	VkDeviceMemory mTextureImageMemory;
	VkImageView mTextureImageView;
	VkDescriptorImageInfo mImageInfo;
	VkDescriptorSetLayoutBinding mDescriptorSetLayout;
	VkWriteDescriptorSet mDescriptorSet;

	uint32_t mMipLevels;
	VkDevice mDevice;

	bool isDestroyed = false;
};