#pragma once

/*	Author: Louis Mayor
	Data: 10/09/18

	Vulkan Texture Sampler
*/

#include "VulkanHelpers.h"

class TextureSampler {
public:
	TextureSampler( ) = default;
	TextureSampler( VkDevice _device, VkFilter _filterMode, VkSamplerAddressMode _addressMode, float _maxLod, VkBool32 _enableAnisotropy, float _maxAnisotropy = 0.f ) {
		Create( _device, _filterMode, _addressMode, _maxLod, _enableAnisotropy, _maxAnisotropy );
	}

	~TextureSampler( ) {
	}

	void Create( VkDevice _device, VkFilter _filterMode, VkSamplerAddressMode _addressMode, float _maxLod, VkBool32 _enableAnisotropy, float _maxAnisotropy = 0.f ) {

		mSamplerInfo.sType                   = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		mSamplerInfo.magFilter               = _filterMode;
		mSamplerInfo.minFilter               = _filterMode;
		mSamplerInfo.addressModeU            = _addressMode;
		mSamplerInfo.addressModeV            = _addressMode;
		mSamplerInfo.addressModeW            = _addressMode;
		mSamplerInfo.anisotropyEnable        = _enableAnisotropy;
		mSamplerInfo.maxAnisotropy           = _maxAnisotropy;
		mSamplerInfo.borderColor             = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
		mSamplerInfo.unnormalizedCoordinates = VK_FALSE;
		mSamplerInfo.compareEnable           = VK_FALSE;
		mSamplerInfo.compareOp               = VK_COMPARE_OP_ALWAYS;
		mSamplerInfo.mipmapMode              = ( _filterMode == VK_FILTER_NEAREST ) ? VK_SAMPLER_MIPMAP_MODE_NEAREST : VK_SAMPLER_MIPMAP_MODE_LINEAR;
		mSamplerInfo.minLod                  = 0;
		mSamplerInfo.maxLod                  = static_cast<float>( _maxLod );
		mSamplerInfo.mipLodBias              = 0;

		if( vkCreateSampler( _device, &mSamplerInfo, nullptr, &mSampler ) != VK_SUCCESS ) {
			throw std::runtime_error( "failed to create texture sampler!" );
		}

		mDevice = _device;
	}

	void Destroy( ) {

		if( !isDestroyed ) {
			if( mSampler != 0 && mDevice != nullptr ) {
				vkDestroySampler( mDevice, mSampler, nullptr );
			}
			isDestroyed = !isDestroyed;
		}
	}

	VkSampler& GetSampler( ) {
		return mSampler;
	}

private:
	VkSampler mSampler;
	VkSamplerCreateInfo mSamplerInfo;
	VkDevice mDevice;

	bool isDestroyed = false;
};