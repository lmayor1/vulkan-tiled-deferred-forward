#pragma once

/*	Author: Louis Mayor
	Data: 13/09/18

	Vulkan Render Target
*/

#include "VulkanHelpers.h"

class RenderTarget {
public:

	RenderTarget( ) = default;
	~RenderTarget( ) {

	}

	RenderTarget( VkPhysicalDevice _physicalDevice, VkDevice _device, VkCommandPool _commandPool, VkQueue _queue,
				 VkFormat format, uint32_t width, uint32_t height, uint32_t mipLevels, VkSampleCountFlagBits numSamples,
				 VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImageLayout _finalLayout ) {

		createImage( _physicalDevice, _device, width, height, 1, numSamples, format,
					 tiling, usage, properties, mColourImage, mColourImageMemory );

		mColourImageView = createImageView( _device, mColourImage, format, VK_IMAGE_ASPECT_COLOR_BIT, 1 );

		transitionImageLayout( _device, _commandPool, _queue, mColourImage, format, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, 1 );

		CreateAttachmentDesc( format, numSamples, _finalLayout );
		CreateResolveAttachmentDesc( format, numSamples );

		mDevice = _device;
	}

	void Create( VkPhysicalDevice _physicalDevice, VkDevice _device, VkCommandPool _commandPool, VkQueue _queue,
				 VkFormat format, uint32_t width, uint32_t height, uint32_t mipLevels, VkSampleCountFlagBits numSamples,
				 VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImageLayout _finalLayout ) {

		createImage( _physicalDevice, _device, width, height, 1, numSamples, format,
					 tiling, usage, properties, mColourImage, mColourImageMemory );

		mColourImageView = createImageView( _device, mColourImage, format, VK_IMAGE_ASPECT_COLOR_BIT, 1 );

		transitionImageLayout( _device, _commandPool, _queue, mColourImage, format, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, 1 );

		CreateAttachmentDesc( format, numSamples, _finalLayout );
		CreateResolveAttachmentDesc( format, numSamples );

		mDevice = _device;
	}

	void Destroy( ) {

		vkDestroyImageView( mDevice, mColourImageView, nullptr );
		vkDestroyImage( mDevice, mColourImage, nullptr );
		vkFreeMemory( mDevice, mColourImageMemory, nullptr );
	}

	VkImageView& GetImageView( ) {
		return mColourImageView;
	}

	VkAttachmentDescription& GetAttachmentDesc( ) {
		return mColourAttachmentDesc;
	}

	VkAttachmentDescription& GetResolveAttachmentDesc( ) {
		return mColourResolveAttachmentDesc;
	}

private:
	void CreateAttachmentDesc( VkFormat format, VkSampleCountFlagBits numSamples, VkImageLayout _finalLayout ) {

		mColourAttachmentDesc.format         = format;
		mColourAttachmentDesc.samples        = numSamples;
		mColourAttachmentDesc.loadOp         = VK_ATTACHMENT_LOAD_OP_CLEAR;
		mColourAttachmentDesc.storeOp        = VK_ATTACHMENT_STORE_OP_STORE;
		mColourAttachmentDesc.stencilLoadOp  = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		mColourAttachmentDesc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		mColourAttachmentDesc.initialLayout  = VK_IMAGE_LAYOUT_UNDEFINED;
		mColourAttachmentDesc.finalLayout    = _finalLayout;
	}

	void CreateResolveAttachmentDesc( VkFormat format, VkSampleCountFlagBits numSamples ) {

		mColourResolveAttachmentDesc.format         = format;
		mColourResolveAttachmentDesc.samples        = VK_SAMPLE_COUNT_1_BIT;
		mColourResolveAttachmentDesc.loadOp         = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		mColourResolveAttachmentDesc.storeOp        = VK_ATTACHMENT_STORE_OP_STORE;
		mColourResolveAttachmentDesc.stencilLoadOp  = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		mColourResolveAttachmentDesc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		mColourResolveAttachmentDesc.initialLayout  = VK_IMAGE_LAYOUT_UNDEFINED;
		mColourResolveAttachmentDesc.finalLayout    = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
	}

private:
	VkImageView mColourImageView;
	VkImage mColourImage;
	VkDeviceMemory mColourImageMemory;

	VkAttachmentDescription mColourAttachmentDesc;
	VkAttachmentDescription mColourResolveAttachmentDesc;

	VkDevice mDevice;
	bool mMultiSampleEnabled;
};