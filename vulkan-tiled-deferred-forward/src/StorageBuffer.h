#pragma once

/*	Author: Louis Mayor
	Data: 10/09/18

	Vulkan Storage Buffer
*/

#include <cassert>
#include "VulkanHelpers.h"

template<typename T>
class StorageBuffer {
public:
	StorageBuffer( ) = default;
	~StorageBuffer( ) {
	}

	StorageBuffer( VkPhysicalDevice const _physicalDevice, VkDevice const _device, const int&& _totalBuffers = 1 ) {

		assert( _totalBuffers >= 1 );
		mDevice          = _device;
		mPhysicalDevice  = _physicalDevice;
		mAmountOfBuffers = _totalBuffers;
		mBufferSize      = sizeof( T );
		isDestroyed      = false;
		mStorageBuffers.resize( _totalBuffers );
		mStorageBuffersMemory.resize( _totalBuffers );
		SetTotalInstances( );
	}

	void Initialise( VkPhysicalDevice const _physicalDevice, VkDevice const _device, const int _totalBuffers = 1 ) {

		assert( _totalBuffers >= 1 );
		mDevice          = _device;
		mPhysicalDevice  = _physicalDevice;
		mAmountOfBuffers = _totalBuffers;
		mBufferSize      = sizeof( T );
		isDestroyed      = false;
		mStorageBuffers.resize( _totalBuffers );
		mStorageBuffersMemory.resize( _totalBuffers );
		SetTotalInstances( );
	}

	void CreateDescriptorSetLayout( const VkShaderStageFlagBits&& _stageFlag, const uint32_t&& _dstBinding = -1 ) {

		mDescriptorSetLayout = createDescriptorSetLayout( VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, _stageFlag, _dstBinding );
	}

	void CreateDescriptorSet( const size_t _bufferIndex, const VkStructureType&& _structType, 
							  const VkDescriptorSet& _descSet, const uint32_t _dstBinding ) {

		mDescriptorSet = createDescriptorSet( _structType, _descSet, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, &GetBufferInfo( _bufferIndex ), nullptr, _dstBinding );
	}

	void UpdateDescriptorSet( ) {
		vkUpdateDescriptorSets( mDevice, 1, &GetDescriptorSet( ), 0, nullptr );
	}

	void AllocateBuffer( const size_t _index ) {

		createBuffer( mPhysicalDevice, mDevice,
					  GetBufferSize( ), VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
					  VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
					  GetBuffer( _index ), GetBufferMemory( _index ) );
	}

	void Map( const int _currentBuffer = 0 ) {

		void* data;
		vkMapMemory( mDevice, GetBufferMemory( _currentBuffer ), 0, static_cast<size_t>( GetBufferSize( ) ), 0, &data );
		memcpy( data, &GetInstance( 0 ), static_cast<size_t>( GetBufferSize( ) ) );
		vkUnmapMemory( mDevice, GetBufferMemory( _currentBuffer ) );
	}

	void Destroy( ) {

		if( !isDestroyed ) {
			for( size_t i = 0; i < mAmountOfBuffers; i++ ) {
				if( mStorageBuffers[i] != 0 && mStorageBuffersMemory[i] != 0 ) {
					vkDestroyBuffer( mDevice, mStorageBuffers[i], nullptr );
					vkFreeMemory( mDevice, mStorageBuffersMemory[i], nullptr );
					mStorageBuffers[i]       = 0;
					mStorageBuffersMemory[i] = 0;
				}
			}
			isDestroyed = !isDestroyed;
		}
	}

	VkDescriptorBufferInfo& GetBufferInfo( const size_t _index = 0 ) {

		mBufferInfo.buffer = GetBuffer();
		mBufferInfo.offset = 0;
		mBufferInfo.range  = GetBufferSize( );

		return mBufferInfo;
	}

	VkBuffer& GetBuffer( const size_t _index = 0 ) {
		return mStorageBuffers[_index];
	}

	VkDeviceMemory& GetBufferMemory( const size_t _index = 0 ) {
		return mStorageBuffersMemory[_index];
	}

	inline VkDeviceSize GetBufferSize( ) const {
		return mBufferSize * mInstances.size( );
	}

	inline VkDescriptorSetLayoutBinding& GetDescriptorLayout( ) {
		return mDescriptorSetLayout;
	}

	inline VkWriteDescriptorSet& GetDescriptorSet( ) {
		return mDescriptorSet;
	}

	void SetTotalInstances( const size_t _index = 1 ) {
		mInstances.clear( );
		mInstances.resize( _index );
	}

	T& GetInstance( const size_t _index = 0 ) {
		return mInstances[_index];
	}

	std::vector<T>& GetInstances( ) {
		return mInstances;
	}

private:
	StorageBuffer( const StorageBuffer& _other )  = delete;
	void operator=( const StorageBuffer& _other ) = delete;

	size_t mAmountOfBuffers;
	std::vector<VkBuffer> mStorageBuffers;
	std::vector<VkDeviceMemory> mStorageBuffersMemory;
	VkBufferUsageFlagBits mBufferUsage;
	VkDeviceSize mBufferSize;
	VkDevice mDevice;
	VkPhysicalDevice mPhysicalDevice;
	VkDescriptorBufferInfo mBufferInfo;
	VkDescriptorSetLayoutBinding mDescriptorSetLayout;
	VkWriteDescriptorSet mDescriptorSet;
	std::vector<T> mInstances;

	bool isDestroyed;
};