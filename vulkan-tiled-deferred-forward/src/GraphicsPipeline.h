#pragma once

#include "VulkanHelpers.h"

extern Logger gLogger;

class GraphicsPipeline {
public:
	GraphicsPipeline( ) : mSetupStages( mStageCount, false ) {

	}

	~GraphicsPipeline( ) {

	}

	bool Destroy( VkDevice _device ) {

		if( mPipeLayout != 0 && mPipeline != 0 ) {
			vkDestroyPipelineLayout( _device, mPipeLayout, nullptr );
			vkDestroyPipeline( _device, mPipeline, nullptr );
			mPipeLayout = 0;
			mPipeline   = 0;
			return true;
		}
		return false;
	}

	void SetInputAssembler( VkVertexInputBindingDescription _bindingDesc, std::vector<VkVertexInputAttributeDescription> _attributeDescs,
							VkPrimitiveTopology _topology, VkBool32 _primRestart ) {

		mBindingDesc	 = _bindingDesc;
		mAttributeDesc	 = _attributeDescs;

		// vertex input
		mVertexInputState.sType                           = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
		mVertexInputState.vertexBindingDescriptionCount   = 1;
		mVertexInputState.pVertexBindingDescriptions      = &mBindingDesc;
		mVertexInputState.vertexAttributeDescriptionCount = mAttributeDesc.size( );
		mVertexInputState.pVertexAttributeDescriptions    = mAttributeDesc.data( );
		// input assembler
		mInputAssemblyState.sType                         = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		mInputAssemblyState.topology                      = _topology;
		mInputAssemblyState.primitiveRestartEnable        = _primRestart;

		mSetupStages[0] = true;
	}

	void SetViewport( VkExtent2D _dims, float _minDepth = 0.f, float _maxDepth = 1.f ) {

		// viewport
		mViewport.x                  = 0.f;
		mViewport.y                  = 0.f;
		mViewport.width              = static_cast<float>( _dims.width  );
		mViewport.height             = static_cast<float>( _dims.height );
		mViewport.minDepth           = _minDepth;
		mViewport.maxDepth           = _maxDepth;
		// scissor
		mScissor.offset              = { 0, 0 };
		mScissor.extent              = _dims;
		// viewport state
		mViewportState.sType         = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		mViewportState.viewportCount = 1;
		mViewportState.pViewports    = &mViewport;
		mViewportState.scissorCount  = 1;
		mViewportState.pScissors     = &mScissor;

		mSetupStages[1] = true;
	}

	void SetRasterizer( VkBool32 _depthWrite, VkBool32 _depthTest,
						VkCompareOp _depthCompare, VkSampleCountFlagBits _multisampleCount,
						VkBool32 _sampleShading, const bool _debug = false ) {

		// rasterizer
		mRasterizerState.sType                   = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		mRasterizerState.depthClampEnable        = VK_FALSE;
		mRasterizerState.rasterizerDiscardEnable = VK_FALSE;
		mRasterizerState.depthBiasEnable         = VK_FALSE;
		mRasterizerState.frontFace               = VK_FRONT_FACE_COUNTER_CLOCKWISE;
		if( _debug ) {
			mRasterizerState.polygonMode = VK_POLYGON_MODE_LINE;
			mRasterizerState.lineWidth   = 1.5f;
			mRasterizerState.cullMode    = VK_CULL_MODE_NONE;
		} else {
			mRasterizerState.polygonMode = VK_POLYGON_MODE_FILL;
			mRasterizerState.lineWidth   = 1.0f;
			mRasterizerState.cullMode    = VK_CULL_MODE_BACK_BIT;
		}
		// multi-sampling
		mMultiSamplingState.sType                = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		mMultiSamplingState.sampleShadingEnable  = _sampleShading;
		mMultiSamplingState.rasterizationSamples = _multisampleCount;
		// depth/stencil
		mDepthStencilState.sType                 = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
		mDepthStencilState.depthTestEnable       = _depthTest;
		mDepthStencilState.depthWriteEnable      = _depthWrite;
		mDepthStencilState.depthCompareOp        = _depthCompare;
		mDepthStencilState.depthBoundsTestEnable = VK_FALSE;
		mDepthStencilState.stencilTestEnable     = VK_FALSE;
		// blending mode(s)
		mBlending.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		mBlending.blendEnable    = VK_FALSE;
		// blending state
		mBlendingState.sType             = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		mBlendingState.logicOpEnable     = VK_FALSE;
		mBlendingState.logicOp           = VK_LOGIC_OP_COPY;
		mBlendingState.attachmentCount   = 1;
		mBlendingState.pAttachments      = &mBlending;
		mBlendingState.blendConstants[0] = 0.0f;
		mBlendingState.blendConstants[1] = 0.0f;
		mBlendingState.blendConstants[2] = 0.0f;
		mBlendingState.blendConstants[3] = 0.0f;

		mSetupStages[2] = true;
	}

	template<typename T>
	void SetPushConstants( size_t _offset, VkShaderStageFlagBits _stage ) {

		mPushConstants.offset     = _offset;
		mPushConstants.size       = sizeof( T );
		mPushConstants.stageFlags = _stage;
	}

	void SetPushConstants( size_t _offset, size_t _size, VkShaderStageFlagBits _stage ) {

		mPushConstants.offset     = _offset;
		mPushConstants.size       = _size;
		mPushConstants.stageFlags = _stage;
	}

	void SetShaderStages( std::vector<VkPipelineShaderStageCreateInfo> _shaders ) {

		mShaderStages = _shaders;
		mSetupStages[3] = true;
	}

	void CreatePipelineLayout( VkDevice _device, VkDescriptorSetLayout* _descriptorSetLayouts, size_t _descriptorSetLayoutCount, size_t _pushConstantsCount ) {

		mPipeLayoutDesc.sType                  = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		mPipeLayoutDesc.setLayoutCount         = _descriptorSetLayoutCount;
		mPipeLayoutDesc.pSetLayouts            = _descriptorSetLayouts;
		mPipeLayoutDesc.pushConstantRangeCount = _pushConstantsCount;
		mPipeLayoutDesc.pPushConstantRanges    = ( _pushConstantsCount > 0 ) ? &mPushConstants : nullptr;

		if( vkCreatePipelineLayout( _device, &mPipeLayoutDesc, nullptr, &mPipeLayout ) != VK_SUCCESS ) {
			throw std::runtime_error( "failed to create pipeline layout!" );
		}

		mSetupStages[4] = true;
	}

	bool CreateGraphicsPipeline( VkDevice _device, VkRenderPass _renderpass ) {

		mSetupComplete = true;
		for( auto stage : mSetupStages ) {
			if( !stage ) {
				mSetupComplete = false;
			}
		}

		if( !mSetupComplete ) {
			gLogger.Error( "Stage in graphics pipeline creation not properly configured", "CreateGraphicsPipeline()", "GraphicsPipeline.h" );
			return false;
		}

		mPipelineDesc.sType               = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		mPipelineDesc.pInputAssemblyState = &mInputAssemblyState;
		mPipelineDesc.pVertexInputState   = &mVertexInputState;
		mPipelineDesc.pViewportState      = &mViewportState;
		mPipelineDesc.pRasterizationState = &mRasterizerState;
		mPipelineDesc.pMultisampleState   = &mMultiSamplingState;
		mPipelineDesc.pDepthStencilState  = &mDepthStencilState;
		mPipelineDesc.pColorBlendState    = &mBlendingState;
		mPipelineDesc.layout              = mPipeLayout;
		mPipelineDesc.stageCount          = mShaderStages.size( );
		mPipelineDesc.pStages             = &mShaderStages[0];
		mPipelineDesc.renderPass          = _renderpass;
		mPipelineDesc.subpass             = 0;
		mPipelineDesc.basePipelineHandle  = VK_NULL_HANDLE;

		if( vkCreateGraphicsPipelines( _device, VK_NULL_HANDLE, 1, &mPipelineDesc, nullptr, &mPipeline ) != VK_SUCCESS ) {
			throw std::runtime_error( "failed to create graphics pipeline!" );
			return false;
		}
		return true;
	}

	VkPipelineLayout GetPipelineLayout( ) const {
		return mPipeLayout;
	}

	VkPipeline GetPipeline( ) const {
		return mPipeline;
	}

private:
	// input/vertex
	VkPipelineInputAssemblyStateCreateInfo mInputAssemblyState = {};
	VkPipelineVertexInputStateCreateInfo mVertexInputState = { };

	VkVertexInputBindingDescription mBindingDesc;
	std::vector<VkVertexInputAttributeDescription> mAttributeDesc;

	// viewport
	VkPipelineViewportStateCreateInfo mViewportState = { };
	VkViewport mViewport = { };
	VkRect2D mScissor = { };

	// rasterizer
	VkPipelineRasterizationStateCreateInfo mRasterizerState;
	VkPipelineMultisampleStateCreateInfo mMultiSamplingState;
	VkPipelineDepthStencilStateCreateInfo mDepthStencilState;
	VkPipelineColorBlendAttachmentState mBlending;
	VkPipelineColorBlendStateCreateInfo mBlendingState;

	// shaders
	std::vector<VkPipelineShaderStageCreateInfo> mShaderStages;

	// push constants
	VkPushConstantRange mPushConstants;

	const int mStageCount = 5;
	std::vector<bool> mSetupStages;
	bool mSetupComplete = false;

	// pipeline
	VkPipelineLayoutCreateInfo mPipeLayoutDesc;
	VkPipelineLayout mPipeLayout;

	VkGraphicsPipelineCreateInfo mPipelineDesc;
	VkPipeline mPipeline;
};