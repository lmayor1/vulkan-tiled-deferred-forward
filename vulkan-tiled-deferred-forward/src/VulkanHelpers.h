#pragma once

/*	Author: Louis Mayor
	Data: 10/09/18

	Vulkan Helper functions
*/

#include <vulkan/vulkan.h>

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <algorithm>
#include <vector>
#include <cstring>
#include <cstdlib>
#include <set>
#include <chrono>

const std::vector<const char*> validationLayers = {
	"VK_LAYER_LUNARG_standard_validation"
};

const std::vector<const char*> deviceExtensions = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif

struct QueueFamilyIndices {
	int graphicsFamily = -1;
	int presentFamily = -1;

	bool isComplete( ) {
		return graphicsFamily >= 0 && presentFamily >= 0;
	}
};

struct SwapChainSupportDetails {
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

static VkCommandBuffer beginSingleTimeCommands( VkDevice _device, VkCommandPool _commandPool ) {

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level              = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandPool        = _commandPool;
	allocInfo.commandBufferCount = 1;

	VkCommandBuffer commandBuffer;
	vkAllocateCommandBuffers( _device, &allocInfo, &commandBuffer );

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer( commandBuffer, &beginInfo );

	return commandBuffer;
}

static void endSingleTimeCommands( VkDevice _device, VkCommandPool _commandPool, VkCommandBuffer _commandBuffer, VkQueue _queue ) {

	vkEndCommandBuffer( _commandBuffer );

	VkSubmitInfo submitInfo = {};
	submitInfo.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers    = &_commandBuffer;

	vkQueueSubmit( _queue, 1, &submitInfo, VK_NULL_HANDLE );
	vkQueueWaitIdle( _queue );

	vkFreeCommandBuffers( _device, _commandPool, 1, &_commandBuffer );
}

static VkDescriptorSetLayoutBinding createDescriptorSetLayout( const VkDescriptorType& _descType, const VkShaderStageFlagBits& _stageFlag,
															   const uint32_t& _dstBinding = -1 ) {

	static int mDescriptorSetLayoutBindingBindingLocation = 0;

	VkDescriptorSetLayoutBinding layoutBinding = {};
	layoutBinding.binding            = ( _dstBinding == -1 ) ? mDescriptorSetLayoutBindingBindingLocation : _dstBinding;
	layoutBinding.descriptorCount    = 1;
	layoutBinding.descriptorType     = _descType;
	layoutBinding.pImmutableSamplers = nullptr;
	layoutBinding.stageFlags         = _stageFlag;

	if( _dstBinding == -1 ) {
		mDescriptorSetLayoutBindingBindingLocation++;
	}
	return layoutBinding;
}

static VkWriteDescriptorSet createDescriptorSet( const VkStructureType& _structType, const VkDescriptorSet& _descSet,
												 const VkDescriptorType& _descType,  const VkDescriptorBufferInfo* _bufferInfo,
												 const VkDescriptorImageInfo* _imageInfo, const uint32_t& _dstBinding = -1 ) {

	static int mDescriptorSetBindingLocation = 0;

	VkWriteDescriptorSet descriptorWrite = { };
	descriptorWrite.sType           = _structType;
	descriptorWrite.dstSet          = _descSet;
	descriptorWrite.dstBinding      = (_dstBinding == -1) ? mDescriptorSetBindingLocation : _dstBinding;
	descriptorWrite.dstArrayElement = 0;
	descriptorWrite.descriptorType  = _descType;
	descriptorWrite.descriptorCount = 1;
	descriptorWrite.pBufferInfo     = _bufferInfo;
	descriptorWrite.pImageInfo      = _imageInfo;

	// potential unintended side-effects (i.e multiple frames being rendered)
	if( _dstBinding == -1 ) {
		mDescriptorSetBindingLocation++;
	}
	return descriptorWrite;
}

static uint32_t findMemoryType( VkPhysicalDevice _physicalDevice, uint32_t _typeFilter, VkMemoryPropertyFlags _properties ) {

	VkPhysicalDeviceMemoryProperties memProperties;
	vkGetPhysicalDeviceMemoryProperties( _physicalDevice, &memProperties );

	for( uint32_t i = 0; i < memProperties.memoryTypeCount; i++ ) {
		if( ( _typeFilter & ( 1 << i ) ) && ( memProperties.memoryTypes[i].propertyFlags & _properties ) == _properties ) {
			return i;
		}
	}

	throw std::runtime_error( "failed to find suitable memory type!" );
}

static bool hasStencilComponent( VkFormat format ) {
	return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

static void createBuffer( VkPhysicalDevice _physicalDevice,
						  VkDevice _device, VkDeviceSize size,
						  VkBufferUsageFlags usage, VkMemoryPropertyFlags properties,
						  VkBuffer& buffer, VkDeviceMemory& bufferMemory ) {

	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = usage;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	if( vkCreateBuffer( _device, &bufferInfo, nullptr, &buffer ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create buffer!" );
	}

	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements( _device, buffer, &memRequirements );

	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = findMemoryType( _physicalDevice, memRequirements.memoryTypeBits, properties );

	if( vkAllocateMemory( _device, &allocInfo, nullptr, &bufferMemory ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to allocate buffer memory!" );
	}

	vkBindBufferMemory( _device, buffer, bufferMemory, 0 );
}

static void transitionImageLayout( VkDevice _device, VkCommandPool _commandPool, VkQueue _queue, VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t mipLevels ) {

	VkCommandBuffer commandBuffer = beginSingleTimeCommands( _device, _commandPool );

	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.oldLayout = oldLayout;
	barrier.newLayout = newLayout;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.image = image;

	if( newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL ) {
		barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

		if( hasStencilComponent( format ) ) {
			barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
		}
	} else {
		barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	}

	barrier.subresourceRange.baseMipLevel = 0;
	barrier.subresourceRange.levelCount = mipLevels;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;

	VkPipelineStageFlags sourceStage;
	VkPipelineStageFlags destinationStage;

	if( oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL ) {
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	} else if( oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL ) {
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	} else if( oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL ) {
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
	} else if( oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL ) {
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destinationStage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	} else {
		throw std::invalid_argument( "unsupported layout transition!" );
	}

	vkCmdPipelineBarrier(
		commandBuffer,
		sourceStage, destinationStage,
		0,
		0, nullptr,
		0, nullptr,
		1, &barrier
	);

	endSingleTimeCommands( _device, _commandPool, commandBuffer, _queue );
}

static void createImage( VkPhysicalDevice _physicalDevice, VkDevice _device,
				  uint32_t width, uint32_t height, uint32_t mipLevels, VkSampleCountFlagBits numSamples,
				  VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties,
				  VkImage& image, VkDeviceMemory& imageMemory ) {

	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent.width = width;
	imageInfo.extent.height = height;
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = mipLevels;
	imageInfo.arrayLayers = 1;
	imageInfo.format = format;
	imageInfo.tiling = tiling;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.usage = usage;
	imageInfo.samples = numSamples;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	if( vkCreateImage( _device, &imageInfo, nullptr, &image ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create image!" );
	}

	VkMemoryRequirements memRequirements;
	vkGetImageMemoryRequirements( _device, image, &memRequirements );

	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = findMemoryType( _physicalDevice, memRequirements.memoryTypeBits, properties );

	if( vkAllocateMemory( _device, &allocInfo, nullptr, &imageMemory ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to allocate image memory!" );
	}

	vkBindImageMemory( _device, image, imageMemory, 0 );
}

static VkImageView createImageView( VkDevice _device, VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels ) {

	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.image = image;
	viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	viewInfo.format = format;
	viewInfo.subresourceRange.aspectMask = aspectFlags;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = mipLevels;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.layerCount = 1;

	VkImageView imageView;
	if( vkCreateImageView( _device, &viewInfo, nullptr, &imageView ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create texture image view!" );
	}

	return imageView;
}

static void copyBuffer( VkDevice device, VkCommandPool commandPool, VkQueue graphicsQueue, VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size ) {

	VkCommandBuffer commandBuffer = beginSingleTimeCommands( device, commandPool );

	VkBufferCopy copyRegion = {};
	copyRegion.size = size;
	vkCmdCopyBuffer( commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion );

	endSingleTimeCommands( device, commandPool, commandBuffer, graphicsQueue );
}

static VkDeviceSize GetMinUBOAlignment( VkPhysicalDevice _physicalDevice ) {

	VkPhysicalDeviceProperties physicalDeviceProperties;
	vkGetPhysicalDeviceProperties( _physicalDevice, &physicalDeviceProperties );
	return physicalDeviceProperties.limits.minUniformBufferOffsetAlignment;
}