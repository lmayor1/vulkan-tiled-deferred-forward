#pragma once

/*	Author: Louis Mayor
	Data: 10/09/18

	Point light
*/

#include <glm/glm.hpp>

class Light {
public:
	Light( ) = default;

	void SetPosition( const glm::vec3 _pos ) {
		mPositionRadius.x = _pos.x;
		mPositionRadius.y = _pos.y;
		mPositionRadius.z = _pos.z;
	}

	void SetRadius( const float _radius ) {
		mPositionRadius.w = _radius;
	}

	void SetColour( const glm::vec4 _col ) {
		mColour = _col;
	}

	inline glm::vec3 GetPosition( ) const {
		return glm::vec3( mPositionRadius.x, mPositionRadius.y, mPositionRadius.z );
	}

	inline float GetRadius( ) const {
		return mPositionRadius.w;
	}

	inline glm::vec4 GetColour( ) const {
		return mColour;
	}

	void operator=( const Light& _light ) {
		mPositionRadius = glm::vec4( _light.GetPosition( ), _light.GetRadius( ) );
		mColour = _light.GetColour( );
	}

private:
	glm::vec4 mPositionRadius = glm::vec4( 0.f, 0.f, 0.f, 5.f );
	glm::vec4 mColour         = glm::vec4( 1.f );
};