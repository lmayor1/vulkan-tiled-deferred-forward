#pragma once

/*	Author: Louis Mayor
	Data: 10/09/18

	Vulkan Application
*/

#include <Windows.h>

#include "VulkanHelpers.h"
#include "Vertex.h"
#include "Shaders.h"
#include "UniformBufferObjects.h"
#include "UniformBuffer.h"
#include "DynamicUniformBuffer.h"
#include "StorageBuffer.h"
#include "TextureSampler.h"
#include "TextureImage.h"
#include "DepthBuffer.h"
#include "RenderTarget.h"
#include "Logger.h"
#include "Model.h"
#include "Camera.h"
#include "RenderPass.h"
#include "FrameBuffer.h"
#include "GraphicsPipeline.h"

#include <random>

#define GLFW_INCLUDE_VULKAN
#include <glfw-3.2.1.bin.WIN32/include/GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/hash.hpp>

#include "Input.h"

enum class eRenderMode {
	Forward,
	TiledForward,
	Deferred,
	TiledDeferred,
	NumOfRenderModes
};

struct AppSettings {
	VkSampleCountFlagBits msaaSamples = VK_SAMPLE_COUNT_1_BIT;

	friend std::ostream& operator<<( std::ostream& _ostream, const AppSettings& _otherApp ) {
		_ostream << std::endl << "MSAA: " << std::to_string( _otherApp.msaaSamples ) << " samples" << std::endl;
		return _ostream;
	}
};

class vk {
public:
	vk( ) = default;

	~vk( ) {

	}

	/* Application functions */

	/* cleanup vulkan application */
	void Cleanup( );

	// TODO: ProcessUI()
	bool ProcessUI( ) = delete;

	/* return false, quit application */
	bool ProcessInput( );

	/* init vulkan application */
	bool Initialise( GLFWwindow* _windowHandle );

	/* update scene */
	void Update( float _delta );

	/* render scene */
	void Render( float _delta );

	/* enable pause of systems */
	void Pause( );

	/* disable pause of systems */
	void UnPause( );

	/* add model to be rendered */
	void AddModelToRenderList( std::unique_ptr<Model> _model );

	// TODO: SetRenderMode()
	void SetRenderMode( const eRenderMode _renderMode ) = delete;

	/* Update application settings */
	void UpdateAppSettings( AppSettings _settings );

	/* Get current application settings */
	AppSettings GetAppSettings( ) const;

	void SetCamera( Camera* const _viewer );

	glm::vec2 GetViewportDimensions( ) const;

private:

	/* Application functions */	

	/* will be expanded once implementing other render modes */
	void PickRenderCommandBuffer( );

	/* will be expanded once implementing other render modes */
	void PickRenderGraphicsPipeline( );

	/* picks and creates appropriate render pass */
	void PickRenderPass( );

	/* picks and creates appropriate render pass */
	void PickFrameBuffer( );

	/* create lights */
	void InitialiseLights( );

	/* re-allocate memory for the light buffer (when adding/removing lights) */
	void RecreateLightBuffer( );

	/* update lights */
	void UpdateLights( float _delta );

	/* add new light */
	void AddLight( std::unique_ptr<Light> _light );

	/* generate light with random position and colour */
	void AddRandomLight( );

	/* remove last light */
	void RemoveLastLight( );

	/* Render methods recording command buffer functions */

	/* (1) Render scene and Compute lighting */
	void RecRenderForwardCmdBuffer( );

	/*.	
		(1) Pre-Z render scene
		(2) Dispatch compute shader and cull lights into a buffer
		(3) Compute lighting and render to screenspace quad
	*/
	void RecRenderTiledForwardCmdBuffer( );

	/*.	
		(1) Render scene to GBuffer
		(2) Compute lighting and render to screenspace quad
	*/
	void RecRenderDeferredCmdBuffer( );

	/*.	
		(1) Render scene to GBuffer
		(2) Dispatch compute shader and cull lights into a buffer
		(3) Compute lighting and render to screenspace quad
	*/
	void RecRenderTiledDeferredCmdBuffer( );

	/* prevent shallow-copy */

	vk( const vk& _other ) = delete;
	void operator=(const vk& _other ) = delete;

	/* Vulkan functions */

	void createForwardGraphicsPipeline( );

	void createGbufferGraphicsPipeline( );

	void createCullLightsGraphicsPipeline( );

	void createSkyboxAndLightingGraphicsPipeline( );

	VkResult CreateDebugUtilsMessengerEXT( VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
										   const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pCallback ) {

		auto func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr( instance, "vkCreateDebugUtilsMessengerEXT" );
		if( func != nullptr ) {
			return func( instance, pCreateInfo, pAllocator, pCallback );
		} else {
			return VK_ERROR_EXTENSION_NOT_PRESENT;
		}
	}

	void DestroyDebugUtilsMessengerEXT( VkInstance instance, VkDebugUtilsMessengerEXT callback, const VkAllocationCallbacks* pAllocator ) {

		auto func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr( instance, "vkDestroyDebugUtilsMessengerEXT" );
		if( func != nullptr ) {
			func( instance, callback, pAllocator );
		}
	}

	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback( VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType,
														 const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData ) {

		std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;
		return VK_FALSE;
	}

	static void framebufferResizeCallback( GLFWwindow* window, int width, int height ) {
		auto app = reinterpret_cast<vk*>( glfwGetWindowUserPointer( window ) );
		app->framebufferResized = true;
	}

	void cleanupSwapChain( );

	void recreateSwapChain( );

	void createInstance( );

	void setupDebugCallback( );

	void createSurface( );

	void pickPhysicalDevice( );

	void createLogicalDevice( );

	void createSwapChain( );

	void createImageViews( );

	void createForwardRenderPass( );

	void createGeometryRenderPass( );

	void createLightRenderPass( );

	void createDescriptorSetLayouts( );

	void createForwardFramebuffers( );

	void createGBufferFramebuffers( );

	void createCommandPool( );

	void CreateCommandBuffer( );

	void createColorResources( );

	void createDepthResources( );

	VkSampleCountFlagBits getMaxUsableSampleCount( VkSampleCountFlagBits _sampleAmount, const bool&& _disable = false );

	void createTextureSamplers( );

	void createTextureImages( );

	void createUniformBuffers( );

	void createStorageBuffers( );

	void createDescriptorPool( );

	void createDescriptorSets( );

	void createSyncObjects( );

	void updateUniformBuffer( uint32_t currentImage );

	void updateStorageBuffer( uint32_t currentImage );

	VkSurfaceFormatKHR chooseSwapSurfaceFormat( const std::vector<VkSurfaceFormatKHR>& availableFormats );

	VkPresentModeKHR chooseSwapPresentMode( const std::vector<VkPresentModeKHR> availablePresentModes );

	VkExtent2D chooseSwapExtent( const VkSurfaceCapabilitiesKHR& capabilities );

	SwapChainSupportDetails querySwapChainSupport( VkPhysicalDevice device );

	bool isDeviceSuitable( VkPhysicalDevice device );

	bool checkDeviceExtensionSupport( VkPhysicalDevice device );

	QueueFamilyIndices findQueueFamilies( VkPhysicalDevice device );

	std::vector<const char*> getRequiredExtensions( );

	bool checkValidationLayerSupport( );

	template<typename T>
	void updatePushConstants( const T& _instance, const VkCommandBuffer _cmdBuffer, const VkPipelineLayout _pipelineLayout, const VkShaderStageFlagBits&& _shaderStageFlag );

	template<typename T>
	void updatePushConstants( const T& _instance, const VkPipelineLayout _pipelineLayout, const VkShaderStageFlagBits&& _shaderStageFlag );

	void updateDynamicUniformBuffer( uint32_t currentImage );

private:
	eRenderMode mRenderMode = eRenderMode::Forward;
	AppSettings mSettings;

	const size_t MAX_FRAMES_IN_FLIGHT = 1;

	GLFWwindow* mWindowHandle;
	float mTimer = 0.0f;
	bool mPause = false;

	int mTotalLights = 0;
	std::vector<std::unique_ptr<Light>> mActiveLights;

	const std::string mShaderDirectory  = "shaders/";
	const std::string mTextureDirectory = "textures/";

	//std::vector<std::string> mModels{
	//	"chalet.obj",
	//	"cube.obj",
	//	"sphere.obj",
	//	"nanosuit.obj"
	//};

	std::vector<std::string> mTextures{
		"chalet.jpg",
		"texture.jpg"
	};

	TextureSampler linearAnisotropicSampler;

	size_t dbgCLight = 0;
	std::vector<glm::vec3> dbgLights = {		
		glm::vec3( 1.5f, 0.f, 0.f ),
		glm::vec3( 0.f, 0.f, 1.f )
	};

	std::vector<glm::vec3> mLightPos = {
		glm::vec3( .7f, .7f, .7f )
	};

	VkInstance instance;
	VkDebugUtilsMessengerEXT callback;
	VkSurfaceKHR surface;

	VkPhysicalDevice physicalDevice   = VK_NULL_HANDLE;
	VkDevice device;

	VkQueue graphicsQueue;
	VkQueue presentQueue;

	VkSwapchainKHR swapChain;
	std::vector<VkImage> swapChainImages;
	VkFormat swapChainImageFormat;
	VkExtent2D swapChainExtent;
	std::vector<VkImageView> swapChainImageViews;	

	std::vector<VkPipelineShaderStageCreateInfo> mShaderStages;

	VkDescriptorSetLayout descriptorSetLayout;

	std::vector<FrameBuffer> mSwapChainFramebuffers;

	RenderPass mForwardRenderPass;
	RenderPass mGbufferRenderPass;

	GraphicsPipeline mForwardPipeline;

	//VkPipelineLayout forwardPipelineLayout;
	VkPipelineLayout gbufferPipelineLayout;

	//VkPipeline forwardGraphicsPipeline;
	VkPipeline gbufferGraphicsPipeline;

	VkPipeline cullLightsGraphicsPipeline;
	VkPipeline skyboxAndLightingGraphicsPipeline;

	VkCommandPool commandPool;

	std::vector<RenderTarget> mGbuffer;

	RenderTarget mBackBuffer;
	DepthBuffer mDepthBuffer;

	UniformBuffer<UBO::CameraMatrces> uniformCameraMat;
	UniformBuffer<UBO::AppData> uniformAppData;
	StorageBuffer<UBO::LightData> storageMultiLightsData;

	VkDescriptorPool descriptorPool;
	std::vector<VkDescriptorSet> descriptorSets;

	std::vector<VkCommandBuffer> commandBuffers;
	std::vector<VkSemaphore> imageAvailableSemaphores;
	std::vector<VkSemaphore> renderFinishedSemaphores;
	std::vector<VkFence> inFlightFences;
	size_t currentFrame = 0;

	bool framebufferResized = false;
	bool mInitData = false;

	std::vector<std::unique_ptr<Model>> mRenderList;

	Camera* mViewer;
};