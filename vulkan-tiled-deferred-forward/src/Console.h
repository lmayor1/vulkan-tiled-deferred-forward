#pragma once

#include <Windows.h>
#include <string>

class Console {
public:
	Console( ) {
		InitializeCriticalSection( );
	}

	~Console( ) {
		DeleteCriticalSection( );
	}

	int Create( std::string _appName ) {

		if( mPipe != INVALID_HANDLE_VALUE ) {
			DisconnectNamedPipe( mPipe );
			CloseHandle( mPipe );
			mPipe = INVALID_HANDLE_VALUE;
		}

		strcpy_s( mConsoleName, "\\\\.\\pipe\\VulkanPipe" );

		mPipe = CreateNamedPipeA
		( 
			mConsoleName,
			PIPE_ACCESS_OUTBOUND, 
			PIPE_TYPE_MESSAGE | PIPE_READMODE_BYTE | PIPE_WAIT,
			1,
			4096,
			0,
			1,
			NULL
		);

		if( mPipe == INVALID_HANDLE_VALUE ) {
			return -1;
		}

		GetStartupInfoA( &__startInfo );

		char cmdline[256];

		sprintf_s( cmdline, "%s %s", "Console.exe", "Logger" );
		BOOL __createProcess = CreateProcessA
		( 
			NULL,
			cmdline,
			NULL,
			NULL,
			FALSE,
			CREATE_NEW_CONSOLE,
			NULL,
			NULL,
			&__startInfo,
			&__processInfo
		);

		BOOL __connectPipe = ConnectNamedPipe( mPipe, NULL ) ?
			TRUE : ( GetLastError( ) == ERROR_PIPE_CONNECTED );

		if( !__connectPipe ) {
			CloseHandle( mPipe );
			mPipe = INVALID_HANDLE_VALUE;
			return -1;
		}
	}

	void Destroy( ) {

		if( mPipe != INVALID_HANDLE_VALUE || mPipe != NULL ) {
			DisconnectNamedPipe( mPipe );
			auto h = OpenProcess( PROCESS_TERMINATE, 0, __processInfo.dwProcessId );
			TerminateProcess( h, 0 );
		}
		return;
	}

	int printf( const char* format, ... ) {

		if( mPipe == INVALID_HANDLE_VALUE ) {
			return -1;
		}			

		int ret;
		char tmp[1024];

		va_list argList;
		va_start( argList, format );
#ifdef WIN32
		ret = _vsnprintf_s( tmp, sizeof( tmp ) - 1, format, argList );
#else
		ret = vsnprintf_s( tmp, sizeof( tmp ) - 1, format, argList );
#endif
		tmp[ret] = 0;

		va_end( argList );

		return _print( tmp, ret );
	}

private:
	int _print( const char *lpszText, int iSize ) {
		DWORD dwWritten = (DWORD) -1;

		return ( (int) dwWritten != iSize ) ? -1 : (int) dwWritten;
	}

	volatile long m_fast_critical_section;

	inline void InitializeCriticalSection( void ) { m_fast_critical_section = 0; }

	inline void DeleteCriticalSection( void ) { m_fast_critical_section = 0; }

private:
	char mConsoleName[64];
	HANDLE mPipe = INVALID_HANDLE_VALUE;

	STARTUPINFOA __startInfo;
	PROCESS_INFORMATION __processInfo;
};