#pragma once

/*	Author: Louis Mayor
	Data: 10/09/18

	Vulkan Shader
*/

#include "VulkanHelpers.h"

class Shader {
public:
	Shader( ) = default;
	Shader( VkDevice _device, const std::string& _directory, const std::string&& _filename, std::string&& _entryPoint = "main" ) {

		readShader( _directory, _filename, _entryPoint );
		createShaderModule( _device );
	}

	~Shader( ) {
		Destroy( );
	}

	void Create( VkDevice _device, const std::string& _directory, const std::string&& _filename, std::string&& _entryPoint = "main" ) {

		readShader( _directory, _filename, _entryPoint );
		createShaderModule( _device );
	}

	/* optional, automatically called in destructor */
	void Destroy( ) {
		if( !isDestroyed ) {
			if( mShaderModule != 0 && mDevice != nullptr ) {
				vkDestroyShaderModule( mDevice, mShaderModule, nullptr );
			}
			isDestroyed = !isDestroyed;
		}		
	}	

	void SetEntryPoint( const std::string& _entryPoint = "main" ) {
		mEntryPoint = _entryPoint;
	}

	void SetEntryPoint( const std::string&& _entryPoint = "main" ) {
		mEntryPoint = _entryPoint;
	}

	std::vector<char> GetShaderCode( ) const {
		return mShaderCode;
	}

	inline VkShaderModule GetShaderModule( ) const {
		return mShaderModule;
	}

	inline std::string& GetEntryPoint( ) {
		return mEntryPoint;
	}

private:
	void createShaderModule( VkDevice _device ) {

		mDevice = _device;

		VkShaderModuleCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		createInfo.codeSize = mShaderCode.size( );
		createInfo.pCode = reinterpret_cast<const uint32_t*>( mShaderCode.data( ) );

		if( vkCreateShaderModule( _device, &createInfo, nullptr, &mShaderModule ) != VK_SUCCESS ) {
			throw std::runtime_error( "failed to create shader module!" );
		}
	}

	void readShader( const std::string& _directory, const std::string& _filename, std::string& _entryPoint ) {

		SetEntryPoint( _entryPoint );

		std::ifstream file( ( _directory + _filename ), std::ios::ate | std::ios::binary );

		if( !file.is_open( ) ) {
			throw std::runtime_error( "failed to open file!" );
		}

		size_t fileSize = (size_t) file.tellg( );
		mShaderCode.resize( fileSize );

		file.seekg( 0 );
		file.read( mShaderCode.data( ), fileSize );

		file.close( );
	}

private:
	Shader( const Shader& _other ) = delete;
	void operator=( const Shader& _other ) = delete;

	std::vector<char> mShaderCode;
	VkShaderModule mShaderModule;
	std::string mEntryPoint;
	VkDevice mDevice;

	bool isDestroyed = false;
};

static VkPipelineShaderStageCreateInfo SetVertex( const VkShaderModule& _shaderModule, const std::string& _entryPoint ) {

	VkPipelineShaderStageCreateInfo shaderStageInfo = {};
	shaderStageInfo.sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	shaderStageInfo.stage  = VK_SHADER_STAGE_VERTEX_BIT;
	shaderStageInfo.module = _shaderModule;
	shaderStageInfo.pName  = _entryPoint.c_str( );

	return shaderStageInfo;
}

static VkPipelineShaderStageCreateInfo SetGeometry( const VkShaderModule& _shaderModule, const std::string& _entryPoint ) {

	VkPipelineShaderStageCreateInfo shaderStageInfo = {};
	shaderStageInfo.sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	shaderStageInfo.stage  = VK_SHADER_STAGE_GEOMETRY_BIT;
	shaderStageInfo.module = _shaderModule;
	shaderStageInfo.pName  = _entryPoint.c_str( );

	return shaderStageInfo;
}

static VkPipelineShaderStageCreateInfo SetFragment( const VkShaderModule& _shaderModule, const std::string& _entryPoint ) {

	VkPipelineShaderStageCreateInfo shaderStageInfo = {};
	shaderStageInfo.sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	shaderStageInfo.stage  = VK_SHADER_STAGE_FRAGMENT_BIT;
	shaderStageInfo.module = _shaderModule;
	shaderStageInfo.pName  = _entryPoint.c_str( );

	return shaderStageInfo;
}

static VkPipelineShaderStageCreateInfo SetCompute( const VkShaderModule& _shaderModule, const std::string& _entryPoint ) {

	VkPipelineShaderStageCreateInfo shaderStageInfo = {};
	shaderStageInfo.sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	shaderStageInfo.stage  = VK_SHADER_STAGE_COMPUTE_BIT;
	shaderStageInfo.module = _shaderModule;
	shaderStageInfo.pName  = _entryPoint.c_str( );

	return shaderStageInfo;
}