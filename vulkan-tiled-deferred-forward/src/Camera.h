#pragma once

/*	Author: Louis Mayor
	Data: 16/09/18

	Camera
*/

#include <glm/glm.hpp>

class Camera {
public:
	Camera( ) {

		mPerpective = glm::mat4x4( 1.f );
		mPosition   = glm::vec3( 0.f );
		mYaw        = 0.0f;
		mPitch      = 0.0f;

		UpdateMatrices( );
	}

	~Camera( ) {

	}

	void UpdateMatrices( ) {

		mForward.x = cos( glm::radians( mYaw ) ) * cos( glm::radians( mPitch ) );
		mForward.y = sin( glm::radians( mPitch ) );
		mForward.z = sin( glm::radians( mYaw ) ) * cos( glm::radians( mPitch ) );
		mForward   = glm::normalize( mForward );

		mRight     = glm::normalize( glm::cross( mForward, glm::vec3( 0.f, 1.f, 0.f ) ) );
		mUp        = glm::normalize( glm::cross( mRight, mForward ) );

		mMatrix    = glm::lookAt( mPosition, mPosition + mForward, mUp );
	}

	void Control( eKeyCodes _movForward, eKeyCodes _movBackward,
				  eKeyCodes _movRight,	 eKeyCodes _movLeft,
				  eKeyCodes _movUp,		 eKeyCodes _movDown,
				  eKeyCodes _rotUp,		 eKeyCodes _rotDown,
				  eKeyCodes _rotRight,	 eKeyCodes _rotLeft,
				  float _delta ) {

		if( KeyHeld( eKeyCodes::ModLSHIFT ) ) {
			mSpeedScale = 2.f;
		} else {
			mSpeedScale = 1.f;
		}

		if( KeyHeld( _movForward ) ) {
			mPosition += mForward * (mMoveSpeed * mSpeedScale ) * _delta;
		}

		if( KeyHeld( _movBackward ) ) {
			mPosition -= mForward * ( mMoveSpeed * mSpeedScale ) * _delta;
		}

		if( KeyHeld( _movRight ) ) {
			mPosition += mRight * ( mMoveSpeed * mSpeedScale ) * _delta;
		}

		if( KeyHeld( _movLeft ) ) {
			mPosition -= mRight * ( mMoveSpeed * mSpeedScale ) * _delta;
		}

		if( KeyHeld( _movUp ) ) {
			mPosition += mUp * ( mMoveSpeed * mSpeedScale ) * _delta;
		}

		if( KeyHeld( _movDown ) ) {
			mPosition -= mUp * ( mMoveSpeed * mSpeedScale ) * _delta;
		}

		if( KeyHeld( _rotUp ) ) {
			mPitch += mRotSpeed * _delta;
		}

		if( KeyHeld( _rotDown ) ) {
			mPitch -= mRotSpeed * _delta;
		}

		if( KeyHeld( _rotRight ) ) {
			mYaw += mRotSpeed * _delta;
		}

		if( KeyHeld( _rotLeft ) ) {
			mYaw -= mRotSpeed * _delta;
		}

		UpdateMatrices( );
	}

	void MouseControl( float _xOffset, float _yOffset ) {

		_xOffset *= mMouseSensitivity;
		_yOffset *= mMouseSensitivity;

		mYaw   += _xOffset;
		mPitch -= _yOffset;

		if( mPitch > 89.9f ) {
			mPitch = 89.9f;
		}

		if( mPitch < -89.9f ) {
			mPitch = -89.9f;
		}

		UpdateMatrices( );
	}

	void SetProjectionMatrix( float _width, float _height, float _fov, float _nearClip, float _farClip ) {

		mPerpective = glm::perspective( glm::radians( _fov ), _width / (float) _height, _nearClip, _farClip );
		mPerpective[1][1] *= -1;
	}

	void SetRotation( glm::vec3 _rotation ) {
		mPitch = _rotation.x;
		mYaw   = _rotation.y;
	}

	void SetPosition( glm::vec3 _position ) {
		mPosition = _position;
	}

	glm::vec3 GetPosition( ) const {
		return mMatrix[3];
	}

	glm::mat4x4 GetViewMatrix( ) const {
		return mMatrix;
	}

	glm::mat4x4 GetProjMatrix( ) const {
		return mPerpective;
	}

private:
	Camera( const Camera& _other ) = delete;
	void operator=( const Camera& _other ) = delete;

	glm::mat4x4 mPerpective;

	glm::vec3 mPosition;
	glm::vec3 mForward;
	glm::vec3 mRight;
	glm::vec3 mUp;
	glm::mat4x4 mMatrix = glm::mat4x4( 1.f );

	float mYaw;
	float mPitch;

	float mSpeedScale       = 1.0f;
	const float mMoveSpeed  = 1.0f;
	const float mRotSpeed   = 10.0f;
	float mMouseSensitivity = 0.1f;
};