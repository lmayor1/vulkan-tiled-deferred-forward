#pragma once

/*	Author: Louis Mayor
	Data: 13/09/18

	Model
*/

#include "VulkanHelpers.h"

#include "Input.h"
#include "Vertex.h"

// macro 'TINYOBJLOADER_IMPLEMENTATION' defined in VulkanApp.cpp
#include <tinyobjloader-master/tiny_obj_loader.h>

extern Logger gLogger;

class Model {
public:
	Model( ) = default;
	~Model( ) {

	}

	void Destroy( ) {

		for( auto& diffuseTex : mDiffuseTextures ) {
			diffuseTex.Destroy( );
		}

		for( auto& indexBuffer : mIndexBuffer ) {
			vkDestroyBuffer( mDevice, indexBuffer, nullptr );
		}

		for( auto& bufferMem : mIndexBufferMemory ) {
			vkFreeMemory( mDevice, bufferMem, nullptr );
		}
		
		for( auto& vertexBuffer : mVertexBuffer ) {
			vkDestroyBuffer( mDevice, vertexBuffer, nullptr );
		}

		for( auto& bufferMem : mVertexBufferMemory ) {
			vkFreeMemory( mDevice, bufferMem, nullptr );
		}
	}

	friend std::ostream& operator<<( std::ostream& _ostream, const Model& _other ) {

		_ostream << std::endl <<
			"Model name: "				<< _other.mModelName		 << std::endl <<
			"Model directory: "			<< _other.mModelDirectory	 << std::endl <<
			"Number of vertices "		<< _other.mTotalVertices	 << std::endl <<
			"Number of indices "		<< _other.mTotalIndices		 << std::endl <<
			"Number of sub-models "		<< _other.mShapes.size( )	 << std::endl;

		for( auto& i : _other.mShapes ) {
			_ostream << i.name << std::endl;
		}

		_ostream << "Number of Materials: "			<< _other.mMaterials.size( ) << std::endl;
		_ostream << "Textures (kD, kS, Normal): "	<< _other.mMaterials.size( ) << std::endl;

		for( auto& i : _other.mMaterials ) {
			if( !i.diffuse_texname.empty( ) ) {
				_ostream << i.diffuse_texname << std::endl;
			}
			if( !i.specular_texname.empty( ) ) {
				_ostream << i.specular_texname << std::endl;
			}
			if( !i.bump_texname.empty( ) ) {
				_ostream << i.bump_texname << std::endl;
			}
		}

		return _ostream;
	}

	bool Load( const std::string _modelDir, const std::string _modelName, const std::string _mtlDir = "" ) {

		mModelDirectory = _modelDir;
		mModelName      = _modelName;

		tinyobj::attrib_t attrib;
		std::string err;
		std::string location = ( _modelDir + '/' + _modelName );

		if( !tinyobj::LoadObj( &attrib, &mShapes, &mMaterials, &err, location.c_str( ), _mtlDir.c_str( ) ) ) {
			throw std::runtime_error( err );
			return false;
		}

		std::unordered_map<Vertex, uint32_t> uniqueVertices = {};

		mVertices.resize( mShapes.size( ) );
		mIndices.resize( mShapes.size( ) );

		mVertexBuffer.resize( mShapes.size( ) );
		mVertexBufferMemory.resize( mShapes.size( ) );

		mIndexBuffer.resize( mShapes.size( ) );
		mIndexBufferMemory.resize( mShapes.size( ) );

		int __shapesIndex = 0;
		for( const auto& shape : mShapes ) {
			for( const auto& index : shape.mesh.indices ) {
				Vertex vertex = {};

				vertex.pos = {
					attrib.vertices[3 * index.vertex_index + 0],
					attrib.vertices[3 * index.vertex_index + 1],
					attrib.vertices[3 * index.vertex_index + 2]
				};

				vertex.normal = {
					attrib.normals[3 * index.normal_index + 0],
					attrib.normals[3 * index.normal_index + 1],
					attrib.normals[3 * index.normal_index + 2]
				};

				vertex.texCoord = {
					attrib.texcoords[2 * index.texcoord_index + 0],
					1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
				};

				vertex.color = { 1.0f, 1.0f, 1.0f };

				if( uniqueVertices.count( vertex ) == 0 ) {
					uniqueVertices[vertex] = static_cast<uint32_t>( mVertices[__shapesIndex].size( ) );
					mVertices[__shapesIndex].push_back( vertex );
				}

				mIndices[__shapesIndex].push_back( uniqueVertices[vertex] );
			}
			__shapesIndex++;
		}

		for( auto& buffer : mVertices ) {
			mTotalVertices += buffer.size( );
		}

		for( auto& buffer : mIndices ) {
			mTotalIndices += buffer.size( );
		}

		mDiffuseTextures.resize( mMaterials.size( ) );

		gLogger.Info<Model>( *this, "Model Loaded" );

		return true;
	}

	bool LoadTextures( VkPhysicalDevice _physicalDevice, VkCommandPool _commandPool, VkQueue _queue ) {

		if( !mMaterials.empty() ) {
			int i = 0;
			for( auto& tex : mDiffuseTextures ) {
				tex.Create( _physicalDevice, mDevice, _commandPool, _queue, mModelDirectory.c_str( ), mMaterials[i].diffuse_texname );
				i++;
			}
		} else {
			mDiffuseTextures.resize( 1 );
			mDiffuseTextures[0].Create( _physicalDevice, mDevice, _commandPool, _queue, "assets/textures", "error.png" );
		}		

		return true;
	}

	void Allocate( VkDevice _device, VkPhysicalDevice _physicalDevice,
				   VkCommandPool _commandPool, VkQueue _graphicsQueue ) {

		mDevice = _device;

		for( size_t models = 0; models < mShapes.size( ); ++models ) {
			createVertexBuffer( _device, _physicalDevice, _commandPool, _graphicsQueue, mVertices[models], mVertexBuffer[models], mVertexBufferMemory[models] );
			createIndexBuffer( _device, _physicalDevice, _commandPool, _graphicsQueue, mIndices[models], mIndexBuffer[models], mIndexBufferMemory[models] );
		}		
	}

	void Render( VkCommandBuffer _commandBuffer ) {

		for( size_t index = 0; index < mShapes.size( ); ++index ) {

			VkBuffer vertexBuffers[] = { mVertexBuffer[index] };
			VkDeviceSize offsets[] = { 0 };

			vkCmdBindVertexBuffers( _commandBuffer, 0, 1, vertexBuffers, offsets );
			vkCmdBindIndexBuffer( _commandBuffer, mIndexBuffer[index], 0, VK_INDEX_TYPE_UINT32 );
			int numOfIndices = static_cast<uint32_t>( mIndices[index].size( ) );
			vkCmdDrawIndexed( _commandBuffer, numOfIndices, 1, 0, 0, 0 );
		}
	}

	void ComputeMatrices( ) {

		glm::mat4x4 __scale = glm::mat4x4( 1.f );
		__scale = glm::scale( __scale, glm::vec3( mScale ) );

		glm::mat4x4 __rotX = glm::mat4x4( 1.f ), 
					__rotY = glm::mat4x4( 1.f ), 
					__rotZ = glm::mat4x4( 1.f );

		__rotX = glm::rotate( __rotX, mRotation.x, glm::vec3( 1, 0, 0 ) );
		__rotY = glm::rotate( __rotY, mRotation.y, glm::vec3( 0, 1, 0 ) );
		__rotZ = glm::rotate( __rotZ, mRotation.z, glm::vec3( 0, 0, 1 ) );

		glm::mat4x4 __trans = glm::mat4x4( 1.f );
		__trans = glm::translate( __trans, mPosition );

		mMatrix = __trans * __rotZ * __rotX * __rotY * __scale;
	}

	void Control( eKeyCodes _movForward, eKeyCodes _movBackward, 
				  eKeyCodes _movRight,	 eKeyCodes _movLeft, 
				  eKeyCodes _movUp,		 eKeyCodes _movDown,
				  eKeyCodes _rotUp,		 eKeyCodes _rotDown,	 
				  eKeyCodes _rotRight,	 eKeyCodes _rotLeft,
				  float _delta ) {

		if( !IsEnabled( ) ) {
			return;
		}

		if( KeyHeld( _movForward ) ) {
			mPosition.z += mMoveSpeed * _delta;
		}

		if( KeyHeld( _movBackward ) ) {
			mPosition.z -= mMoveSpeed * _delta;
		}

		if( KeyHeld( _movRight ) ) {
			mPosition.x += mMoveSpeed * _delta;
		}

		if( KeyHeld( _movLeft ) ) {
			mPosition.x -= mMoveSpeed * _delta;
		}

		if( KeyHeld( _movUp ) ) {
			mPosition.y += mMoveSpeed * _delta;
		}

		if( KeyHeld( _movDown ) ) {
			mPosition.y -= mMoveSpeed * _delta;
		}

		if( KeyHeld( _rotUp ) ) {
			mRotation.x += mRotSpeed * _delta;
		}

		if( KeyHeld( _rotDown ) ) {
			mRotation.x -= mRotSpeed * _delta;
		}

		if( KeyHeld( _rotRight ) ) {
			mRotation.y += mRotSpeed * _delta;
		}

		if( KeyHeld( _rotLeft ) ) {
			mRotation.y -= mRotSpeed * _delta;
		}

		ComputeMatrices( );
	}

	void SetScale( float _scale ) {

		mScale = _scale;

		ComputeMatrices( );
	}

	void SetPosition( glm::vec3 _position ) {

		mPosition = _position;

		ComputeMatrices( );
	}

	glm::vec3 GetPosition( ) const {
		return mMatrix[3];
	}

	glm::mat4x4 GetMatrix( ) const {
		return mMatrix;
	}

	size_t GetNumberOfDiffuseTextures( ) const {
		return mDiffuseTextures.size( );
	}

	bool HasDiffuseTextures( ) const {
		return ( mDiffuseTextures.size( ) > 0 );
	}

	TextureImage& GetDiffuseTexture( const size_t _index ) {
		return mDiffuseTextures[_index];
	}

	std::vector<TextureImage>& GetDiffuseTextures( ) {
		return mDiffuseTextures;
	}

	void EnableControl( const bool _enable ) {
		mEnableControl = _enable;
	}

	inline bool IsEnabled( ) const {
		return mEnableControl;
	}

private:
	void createVertexBuffer( VkDevice _device, VkPhysicalDevice _physicalDevice,
							 VkCommandPool _commandPool, VkQueue _graphicsQueue,
							 std::vector<Vertex>& _Vertices, VkBuffer& _vertexBuffer, VkDeviceMemory& _vertexMemory ) {

		VkDeviceSize bufferSize = sizeof( _Vertices[0] ) * _Vertices.size( );

		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		createBuffer( _physicalDevice, _device, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
					  VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
					  stagingBuffer, stagingBufferMemory );

		void* data;
		vkMapMemory( _device, stagingBufferMemory, 0, bufferSize, 0, &data );
		memcpy( data, _Vertices.data( ), (size_t) bufferSize );
		vkUnmapMemory( _device, stagingBufferMemory );

		createBuffer( _physicalDevice, _device, bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
					  VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
					  _vertexBuffer, _vertexMemory );

		copyBuffer( _device, _commandPool, _graphicsQueue, stagingBuffer, _vertexBuffer, bufferSize );

		vkDestroyBuffer( _device, stagingBuffer, nullptr );
		vkFreeMemory( _device, stagingBufferMemory, nullptr );
	}

	void createIndexBuffer( VkDevice _device, VkPhysicalDevice _physicalDevice,
							VkCommandPool _commandPool, VkQueue _graphicsQueue,
							std::vector<uint32_t>& _Indices, VkBuffer& _indexBuffer, VkDeviceMemory& _indexMemory ) {

		VkDeviceSize bufferSize = sizeof( _Indices[0] ) * _Indices.size( );

		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		createBuffer( _physicalDevice, _device, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
					  VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
					  stagingBuffer, stagingBufferMemory );

		void* data;
		vkMapMemory( _device, stagingBufferMemory, 0, bufferSize, 0, &data );
		memcpy( data, _Indices.data( ), (size_t) bufferSize );
		vkUnmapMemory( _device, stagingBufferMemory );

		createBuffer( _physicalDevice, _device, bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
					  VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
					  _indexBuffer, _indexMemory );

		copyBuffer( _device, _commandPool, _graphicsQueue, stagingBuffer, _indexBuffer, bufferSize );

		vkDestroyBuffer( _device, stagingBuffer, nullptr );
		vkFreeMemory( _device, stagingBufferMemory, nullptr );
	}

private:
	std::string mModelDirectory;
	std::string mModelName;

	std::vector<tinyobj::material_t> mMaterials;
	std::vector<TextureImage>		 mDiffuseTextures;
	std::vector<tinyobj::shape_t>	 mShapes;

	std::vector<std::vector<Vertex>>	mVertices;
	std::vector<std::vector<uint32_t>>	mIndices;

	size_t mTotalVertices;
	size_t mTotalIndices;

	std::vector<VkBuffer>		mVertexBuffer;
	std::vector<VkDeviceMemory> mVertexBufferMemory;

	std::vector<VkBuffer>		mIndexBuffer;
	std::vector<VkDeviceMemory> mIndexBufferMemory;

	VkDevice mDevice;

	/* transforms */
	const float mMoveSpeed = 5.0f;
	const float mRotSpeed  = 1.0f;

	glm::vec3	mPosition    = glm::vec3( 0.f );
	glm::vec3   mRotation	 = glm::vec3( 0.f );
	glm::mat4x4 mMatrix      = glm::mat4x4( 1.f );

	float mScale             = 1.f;

	bool mEnableControl = true;
};