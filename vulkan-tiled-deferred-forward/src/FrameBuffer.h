#pragma once

#include "VulkanHelpers.h"

class FrameBuffer {
public:
	FrameBuffer( ) = default;
	~FrameBuffer( ) {

	}

	void Initialise( const std::vector<VkImageView> _attachments, const VkRenderPass _renderpass,
					 const VkExtent2D _dims, const size_t _layerCount, VkDevice const _device ) {

		mFrameBufferInfo.sType           = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		mFrameBufferInfo.renderPass      = _renderpass;
		mFrameBufferInfo.attachmentCount = _attachments.size( );
		mFrameBufferInfo.pAttachments    = _attachments.data( );
		mFrameBufferInfo.width           = _dims.width;
		mFrameBufferInfo.height          = _dims.height;
		mFrameBufferInfo.layers          = _layerCount;

		if( vkCreateFramebuffer( _device, &mFrameBufferInfo, nullptr, &mFrameBuffer ) != VK_SUCCESS ) {
			throw std::runtime_error( "failed to create framebuffer!" );
		}
	}

	bool Destroy( VkDevice const _device ) {
		if( mFrameBuffer != 0 ) {
			vkDestroyFramebuffer( _device, mFrameBuffer, nullptr );
			mFrameBuffer = 0;
			return true;
		}
		return false;
	}

	VkFramebuffer GetFrameBuffer( ) const {
		return mFrameBuffer;
	}

private:
	VkFramebufferCreateInfo		mFrameBufferInfo = {};
	VkFramebuffer				mFrameBuffer;
};