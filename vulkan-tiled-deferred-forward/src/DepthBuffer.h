#pragma once

/*	Author: Louis Mayor
	Data: 13/09/18

	Vulkan Depth Buffer
*/

#include "VulkanHelpers.h"

class DepthBuffer {
public:

	DepthBuffer( ) = default;
	~DepthBuffer( ) {

	}

	DepthBuffer( VkPhysicalDevice _physicalDevice, VkDevice _device, VkCommandPool _commandPool, VkQueue _queue,
				 uint32_t width, uint32_t height, uint32_t mipLevels, VkSampleCountFlagBits numSamples,
				 VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties ) {

		findDepthFormat( _physicalDevice );

		createImage( _physicalDevice, _device, width, height, 1, numSamples, GetFormat( ),
					 VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
					 mDepthImage, mDepthImageMemory );

		mDepthImageView = createImageView( _device, mDepthImage, GetFormat( ), VK_IMAGE_ASPECT_DEPTH_BIT, 1 );

		transitionImageLayout( _device, _commandPool, _queue, mDepthImage, GetFormat( ), VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, 1 );

		CreateAttachmentDesc( GetFormat( ), numSamples );

		mDevice = _device;
	}

	void Create( VkPhysicalDevice _physicalDevice, VkDevice _device, VkCommandPool _commandPool, VkQueue _queue,
		uint32_t width, uint32_t height, uint32_t mipLevels, VkSampleCountFlagBits numSamples,
		VkImageTiling tiling, VkMemoryPropertyFlags properties ) {

		findDepthFormat( _physicalDevice );

		createImage( _physicalDevice, _device, width, height, 1, numSamples, GetFormat( ),
					 tiling, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, properties,
					 mDepthImage, mDepthImageMemory );

		mDepthImageView = createImageView( _device, mDepthImage, GetFormat( ), VK_IMAGE_ASPECT_DEPTH_BIT, 1 );

		transitionImageLayout( _device, _commandPool, _queue, mDepthImage, GetFormat( ), VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, 1 );

		CreateAttachmentDesc( GetFormat( ), numSamples );

		mDevice = _device;
	}

	void Destroy( ) {

		vkDestroyImageView( mDevice, mDepthImageView, nullptr );
		vkDestroyImage( mDevice, mDepthImage, nullptr );
		vkFreeMemory( mDevice, mDepthImageMemory, nullptr );
	}

	VkFormat& GetFormat( ) {
		return mDepthFormat;
	}

	VkImageView& GetImageView( ) {
		return mDepthImageView;
	}

	VkAttachmentDescription& GetAttachmentDesc( ) {
		return mDepthAttachmentDesc;
	}

private:

	VkFormat findSupportedFormat( VkPhysicalDevice _physicalDevice, const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features ) {

		for( VkFormat format : candidates ) {
			VkFormatProperties props;
			vkGetPhysicalDeviceFormatProperties( _physicalDevice, format, &props );

			if( tiling == VK_IMAGE_TILING_LINEAR && ( props.linearTilingFeatures & features ) == features ) {
				return format;
			} else if( tiling == VK_IMAGE_TILING_OPTIMAL && ( props.optimalTilingFeatures & features ) == features ) {
				return format;
			}
		}

		throw std::runtime_error( "failed to find supported format!" );
	}

	void findDepthFormat( VkPhysicalDevice _physicalDevice ) {

		mDepthFormat = findSupportedFormat( _physicalDevice,
			{ VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
			VK_IMAGE_TILING_OPTIMAL,
			VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
		);
	}

	void CreateAttachmentDesc( VkFormat format, VkSampleCountFlagBits numSamples ) {

		mDepthAttachmentDesc.format         = format;
		mDepthAttachmentDesc.samples        = numSamples;
		mDepthAttachmentDesc.loadOp         = VK_ATTACHMENT_LOAD_OP_CLEAR;
		mDepthAttachmentDesc.storeOp        = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		mDepthAttachmentDesc.stencilLoadOp  = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		mDepthAttachmentDesc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		mDepthAttachmentDesc.initialLayout  = VK_IMAGE_LAYOUT_UNDEFINED;
		mDepthAttachmentDesc.finalLayout    = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
	}

private:
	VkImage mDepthImage;
	VkDeviceMemory mDepthImageMemory;
	VkImageView mDepthImageView;

	VkAttachmentDescription mDepthAttachmentDesc;
	VkFormat mDepthFormat;

	VkDevice mDevice;
};