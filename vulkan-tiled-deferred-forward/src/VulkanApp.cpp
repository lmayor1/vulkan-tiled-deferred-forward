#include "VulkanApp.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb-master/stb_image.h>

#define TINYOBJLOADER_IMPLEMENTATION
#include <tinyobjloader-master/tiny_obj_loader.h>

extern Logger gLogger;

bool vk::Initialise( GLFWwindow* _windowHandle ) {

	mWindowHandle = _windowHandle;

	InitialiseLights( );

	createInstance( );
	setupDebugCallback( );
	createSurface( );
	pickPhysicalDevice( );
	createLogicalDevice( );
	createSwapChain( );
	createImageViews( );
	createCommandPool( );
	createColorResources( );
	createDepthResources( );

	return true;
}

glm::vec2 vk::GetViewportDimensions( ) const {
	return glm::vec2( swapChainExtent.width, swapChainExtent.height );
}

void vk::SetCamera( Camera* const _viewer ) {

	mViewer = _viewer;
}

void vk::Pause( ) {
	mPause = true;
}

void vk::UnPause( ) {
	mPause = false;
}

bool vk::ProcessInput( ) {

	static float scale = 0.05f;
	if( KeyHit ( eKeyCodes::KeyKPAdd ) ) {
		scale += 0.1f;
		mRenderList[0]->SetScale( scale );
	}

	if( KeyHit( eKeyCodes::KeyKPMinus ) ) {
		scale -= 0.1f;
		mRenderList[0]->SetScale( scale );
	}	

	if( KeyHit( eKeyCodes::KeyEsc ) ) {
		return false;
	}

	if( KeyHit( eKeyCodes::KeyP ) ) {
		mPause ? UnPause( ) : Pause( );
	}

	if( KeyHit( eKeyCodes::Key1 ) ) {
		AppSettings __settings = GetAppSettings( );
		__settings.msaaSamples = VK_SAMPLE_COUNT_1_BIT;
		UpdateAppSettings( __settings );
	}

	if( KeyHit( eKeyCodes::Key2 ) ) {
		AppSettings __settings = GetAppSettings( );
		__settings.msaaSamples = VK_SAMPLE_COUNT_2_BIT;
		UpdateAppSettings( __settings );
	}

	if( KeyHit( eKeyCodes::Key3 ) ) {
		AppSettings __settings = GetAppSettings( );
		__settings.msaaSamples = VK_SAMPLE_COUNT_4_BIT;
		UpdateAppSettings( __settings );
	}

	//if( KeyHit( eKeyCodes::KeyKPAdd ) ) {
	//	if( dbgCLight < dbgLights.size( ) ) {
	//		std::unique_ptr<Light> __tmpLight = std::make_unique<Light>( );
	//		__tmpLight->SetPosition( dbgLights[dbgCLight] );
	//		AddLight( std::move( __tmpLight ) );
	//		dbgCLight++;
	//	}		
	//}

	//if( KeyHit( eKeyCodes::KeyKPMinus ) ) {
	//	if( dbgCLight > 0 ) {
	//		RemoveLastLight( );
	//		dbgCLight--;
	//	}
	//}

	return true;
}

void vk::Cleanup( ) {

	// Warning
	vkDeviceWaitIdle( device );

	cleanupSwapChain( );

	vkDestroyDescriptorPool( device, descriptorPool, nullptr );

	vkDestroyDescriptorSetLayout( device, descriptorSetLayout, nullptr );

	linearAnisotropicSampler.Destroy( );

	uniformCameraMat.Destroy( );
	uniformAppData.Destroy( );
	storageMultiLightsData.Destroy( );

	for( auto& model : mRenderList ) {
		model->Destroy( );
	}

	for( size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++ ) {
		if( renderFinishedSemaphores.size() > 0 && imageAvailableSemaphores.size() > 0 && inFlightFences.size() > 0 ) {
			vkDestroySemaphore( device, renderFinishedSemaphores[i], nullptr );
			vkDestroySemaphore( device, imageAvailableSemaphores[i], nullptr );
			vkDestroyFence( device, inFlightFences[i], nullptr );
		} else {
			break;
		}
	}

	vkDestroyCommandPool( device, commandPool, nullptr );

	vkDestroyDevice( device, nullptr );

	if( enableValidationLayers ) {
		DestroyDebugUtilsMessengerEXT( instance, callback, nullptr );
	}

	vkDestroySurfaceKHR( instance, surface, nullptr );
	vkDestroyInstance( instance, nullptr );
}

void vk::cleanupSwapChain( ) {

	mBackBuffer.Destroy( );
	mDepthBuffer.Destroy( );

	for( auto& buffer : mGbuffer ) {
		buffer.Destroy( );
	}

	for( auto& buffer : mSwapChainFramebuffers ) {
		buffer.Destroy( device );
	}

	vkFreeCommandBuffers( device, commandPool, static_cast<uint32_t>( commandBuffers.size( ) ), commandBuffers.data( ) );

	mForwardPipeline.Destroy( device );

	if( gbufferPipelineLayout != 0 ) {
		vkDestroyPipelineLayout( device, gbufferPipelineLayout, nullptr );
	}

	if( gbufferGraphicsPipeline != 0 ) {
		vkDestroyPipeline( device, gbufferGraphicsPipeline, nullptr );
	}

	// render passes
	mForwardRenderPass.Destroy( device );
	mGbufferRenderPass.Destroy( device );

	for( auto imageView : swapChainImageViews ) {
		vkDestroyImageView( device, imageView, nullptr );
	}

	vkDestroySwapchainKHR( device, swapChain, nullptr );
}

void vk::recreateSwapChain( ) {

	int width = 0, height = 0;
	while( width == 0 || height == 0 ) {
		glfwGetFramebufferSize( mWindowHandle, &width, &height );
		glfwWaitEvents( );
	}

	// Warning
	vkDeviceWaitIdle( device );

	cleanupSwapChain( );

	createSwapChain( );
	createImageViews( );
	createColorResources( );
	createDepthResources( );
	PickRenderPass( );
	PickFrameBuffer( );
	CreateCommandBuffer( );
	PickRenderGraphicsPipeline( );	

	mViewer->SetProjectionMatrix( static_cast<float>( swapChainExtent.width ), static_cast<float>( swapChainExtent.height ), 70.f, 0.1f, 1000.0f );
}

AppSettings vk::GetAppSettings( ) const {
	return mSettings;
}

void vk::UpdateAppSettings( AppSettings _settings ) {

	mSettings = _settings;
	
	gLogger.Info<AppSettings>( _settings, "App settings updated" );

	recreateSwapChain( );
}

void vk::PickRenderPass( ) {

	switch( mRenderMode ) {
	case eRenderMode::Forward:
		createForwardRenderPass( );
		break;
	case eRenderMode::TiledForward:
		break;
	case eRenderMode::Deferred:
		break;
	case eRenderMode::TiledDeferred:
		break;
	default:
		break;
	}
}

void vk::PickFrameBuffer( ) {

	switch( mRenderMode ) {
	case eRenderMode::Forward:
		createForwardFramebuffers( );
		break;
	case eRenderMode::TiledForward:
		break;
	case eRenderMode::Deferred:
		break;
	case eRenderMode::TiledDeferred:
		break;
	default:
		break;
	}
}

void vk::PickRenderGraphicsPipeline( ) {

	switch( mRenderMode ) {
	case eRenderMode::Forward:		
		createForwardGraphicsPipeline( );
		break;
	case eRenderMode::TiledForward:
		break;
	case eRenderMode::Deferred:
		break;
	case eRenderMode::TiledDeferred:
		break;
	default:
		break;
	}
}

void vk::PickRenderCommandBuffer( ) {

	switch( mRenderMode ) {
	case eRenderMode::Forward:
		RecRenderForwardCmdBuffer( );
		break;
	case eRenderMode::TiledForward:
		break;
	case eRenderMode::Deferred:
		break;
	case eRenderMode::TiledDeferred:
		break;
	default:
		break;
	}
}

void vk::AddModelToRenderList( std::unique_ptr<Model> _model ) {

	_model->Allocate( device, physicalDevice, commandPool, graphicsQueue );
	mRenderList.push_back( std::move( _model ) );
	mInitData = false;
}

void vk::InitialiseLights( ) {

	mTotalLights = mLightPos.size( );

	// create lights
	int i = 0;
	mActiveLights.resize( mTotalLights );
	for( auto& light : mActiveLights ) {
		light = std::make_unique<Light>( );
		light->SetPosition( mLightPos[i] );
		i++;
	}
}

void vk::AddLight( std::unique_ptr<Light> _light ) {

	gLogger.Info( "Added light" );

	mLightPos.push_back( _light->GetPosition( ) );
	mTotalLights = mLightPos.size( );
	mActiveLights.push_back( std::move( _light ) );

	// Warning
	vkDeviceWaitIdle( device );

	RecreateLightBuffer( );
	PickRenderCommandBuffer( );
}

void vk::AddRandomLight( ) {

	// random number gen
	std::random_device rd;
	std::mt19937 eng( rd( ) );

	// pos
	std::uniform_real_distribution<> distrPos_x( -20.f, 20.f );
	std::uniform_real_distribution<> distrPos_y( -5.f,  5.f  );
	std::uniform_real_distribution<> distrPos_z( -20.f, 20.f );

	// col
	std::uniform_real_distribution<> distrCol_r( 0.f, 1.f );
	std::uniform_real_distribution<> distrCol_g( 0.f, 1.f );
	std::uniform_real_distribution<> distrCol_b( 0.f, 1.f );

	std::unique_ptr<Light> _light = std::make_unique<Light>( );
	_light->SetPosition( glm::vec3( distrPos_x( eng ), distrPos_y( eng ), distrPos_z( eng ) ) );
	_light->SetColour( glm::vec4( distrCol_r( eng ), distrCol_g( eng ), distrCol_b( eng ), 1.f ) );

	mLightPos.push_back( _light->GetPosition( ) );
	mTotalLights = mLightPos.size( );
	mActiveLights.push_back( std::move( _light ) );

	// Warning
	vkDeviceWaitIdle( device );

	RecreateLightBuffer( );
	PickRenderCommandBuffer( );
}

void vk::RemoveLastLight( ) {

	if( mLightPos.size() > 1 ) {
		gLogger.Info( "Removed last light" );

		mLightPos.pop_back( );
		mTotalLights = mLightPos.size( );
		mActiveLights.pop_back( );

		// Warning
		vkDeviceWaitIdle( device );

		RecreateLightBuffer( );
		PickRenderCommandBuffer( );
	}	
}

void vk::UpdateLights( float _delta ) {

}

void vk::Update( float _delta ) {

	/*	after device setup and loaded all models then create sets/bindings/commands
		would re-trigger say, if you changed render mode to another technique or 
		removed/added models (but would require knowing something changed
		but NOT executed in AddedModelToRenderList() for example
	*/
	if( !mInitData ) {
		// Warning
		vkDeviceWaitIdle( device );

		PickRenderPass( );
		PickFrameBuffer( );
		createTextureImages( );
		createTextureSamplers( );
		createUniformBuffers( );
		createStorageBuffers( );
		createDescriptorSetLayouts( );
		createDescriptorPool( );
		createDescriptorSets( );
		CreateCommandBuffer( );
		PickRenderGraphicsPipeline( );
		createSyncObjects( );
		mInitData = true;
	}

	if( !mPause ) {
		mTimer += _delta;
		UpdateLights( _delta );

		mRenderList[0]->Control( eKeyCodes::KeyKP8,		eKeyCodes::KeyKP2,	eKeyCodes::KeyKP6,	eKeyCodes::KeyKP4,
								 eKeyCodes::KeyKP9,		eKeyCodes::KeyKP3,	eKeyCodes::ArrowUp, eKeyCodes::ArrowDown,
								 eKeyCodes::ArrowRight, eKeyCodes::ArrowLeft, _delta );

		mViewer->Control( eKeyCodes::KeyW,		 eKeyCodes::KeyS,		 eKeyCodes::KeyD,	 eKeyCodes::KeyA,
						  eKeyCodes::KeySpace,	 eKeyCodes::ModLCTRL,	 eKeyCodes::ArrowUp, eKeyCodes::ArrowDown,
						  eKeyCodes::ArrowRight, eKeyCodes::ArrowLeft,	 _delta );
	}
}

void vk::Render( float _delta ) {

	// Warning
	vkDeviceWaitIdle( device );

	PickRenderCommandBuffer( );

	vkWaitForFences( device, 1, &inFlightFences[currentFrame], VK_TRUE, std::numeric_limits<uint64_t>::max( ) );

	uint32_t imageIndex;
	VkResult result = vkAcquireNextImageKHR( device, swapChain, std::numeric_limits<uint64_t>::max( ), imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, &imageIndex );

	if( result == VK_ERROR_OUT_OF_DATE_KHR ) {
		recreateSwapChain( );
		return;
	} else if( result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR ) {
		throw std::runtime_error( "failed to acquire swap chain image!" );
	}

	updateDynamicUniformBuffer( imageIndex );
	updateUniformBuffer( imageIndex );
	updateStorageBuffer( imageIndex );

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

	VkSemaphore waitSemaphores[] = { imageAvailableSemaphores[currentFrame] };
	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores    = waitSemaphores;
	submitInfo.pWaitDstStageMask  = waitStages;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers    = &commandBuffers[imageIndex];

	VkSemaphore signalSemaphores[] = { renderFinishedSemaphores[currentFrame] };
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores    = signalSemaphores;

	vkResetFences( device, 1, &inFlightFences[currentFrame] );

	if( vkQueueSubmit( graphicsQueue, 1, &submitInfo, inFlightFences[currentFrame] ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to submit draw command buffer!" );
	}

	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType              = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores    = signalSemaphores;

	VkSwapchainKHR swapChains[] = { swapChain };
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains    = swapChains;
	presentInfo.pImageIndices  = &imageIndex;

	result = vkQueuePresentKHR( presentQueue, &presentInfo );

	if( result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || framebufferResized ) {
		framebufferResized = false;
		recreateSwapChain( );
	} else if( result != VK_SUCCESS ) {
		throw std::runtime_error( "failed to present swap chain image!" );
	}

	currentFrame = ( currentFrame + 1 ) % MAX_FRAMES_IN_FLIGHT;
}

void vk::CreateCommandBuffer( ) {

	commandBuffers.resize( mSwapChainFramebuffers.size( ) );

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool        = commandPool;
	allocInfo.level              = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = (uint32_t) commandBuffers.size( );	

	if( vkAllocateCommandBuffers( device, &allocInfo, commandBuffers.data( ) ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to allocate command buffers!" );
	}
}

void vk::RecRenderForwardCmdBuffer( ) {

	for( size_t i = 0; i < commandBuffers.size( ); i++ ) {
		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

		if( vkBeginCommandBuffer( commandBuffers[i], &beginInfo ) != VK_SUCCESS ) {
			throw std::runtime_error( "failed to begin recording command buffer!" );
		}

		VkRenderPassBeginInfo renderPassInfo = {};
		renderPassInfo.sType             = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.renderPass        = mForwardRenderPass.GetRenderPass( );
		renderPassInfo.framebuffer       = mSwapChainFramebuffers[i].GetFrameBuffer( );
		renderPassInfo.renderArea.offset = { 0, 0 };
		renderPassInfo.renderArea.extent = swapChainExtent;

		std::array<VkClearValue, 2> clearValues = {};
		clearValues[0].color        = { 0.0f, 0.0f, 0.0f, 1.0f };
		clearValues[1].depthStencil = { 1.0f, 0 };

		renderPassInfo.clearValueCount = static_cast<uint32_t>( clearValues.size( ) );
		renderPassInfo.pClearValues    = clearValues.data( );

		vkCmdBeginRenderPass( commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE );

		vkCmdBindPipeline( commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, mForwardPipeline.GetPipeline( ) );

		for( size_t j = 0; j < mRenderList.size( ); ++j ) {

			updatePushConstants<glm::mat4x4>( mRenderList[j]->GetMatrix( ), commandBuffers[i], mForwardPipeline.GetPipelineLayout( ), VK_SHADER_STAGE_VERTEX_BIT );
			vkCmdBindDescriptorSets( commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, mForwardPipeline.GetPipelineLayout( ), 0, 1, &descriptorSets[i], 0, nullptr );
			mRenderList[j]->Render( commandBuffers[i] );
		}

		vkCmdEndRenderPass( commandBuffers[i] );

		if( vkEndCommandBuffer( commandBuffers[i] ) != VK_SUCCESS ) {
			throw std::runtime_error( "failed to record command buffer!" );
		}
	}
}

void vk::RecRenderDeferredCmdBuffer( ) {

	// Render scene to GBuffer

	// Compute lighting and render to screenspace quad
}

void vk::RecRenderTiledForwardCmdBuffer( ) {

	// Pre-Z render scene

	// Dispatch compute shader and cull lights into buffer

	// Compute lighting and render to screenspace quad
}

void vk::RecRenderTiledDeferredCmdBuffer( ) {

	// Render scene to GBuffer

	// Dispatch compute shader and cull lights into buffer

	// Compute lighting and render to screenspace quad
}

void vk::createInstance( ) {

	if( enableValidationLayers && !checkValidationLayerSupport( ) ) {
		throw std::runtime_error( "validation layers requested, but not available!" );
	}

	VkApplicationInfo appInfo = {};
	appInfo.sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName   = "A Vulkan Renderer";
	appInfo.applicationVersion = VK_MAKE_VERSION( 1, 0, 0 );
	appInfo.pEngineName        = "No Engine";
	appInfo.engineVersion      = VK_MAKE_VERSION( 1, 0, 0 );
	appInfo.apiVersion         = VK_API_VERSION_1_0;

	VkInstanceCreateInfo createInfo = {};
	createInfo.sType            = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;

	auto extensions = getRequiredExtensions( );
	createInfo.enabledExtensionCount   = static_cast<uint32_t>( extensions.size( ) );
	createInfo.ppEnabledExtensionNames = extensions.data( );

	if( enableValidationLayers ) {
		createInfo.enabledLayerCount   = static_cast<uint32_t>( validationLayers.size( ) );
		createInfo.ppEnabledLayerNames = validationLayers.data( );
	} else {
		createInfo.enabledLayerCount = 0;
	}

	if( vkCreateInstance( &createInfo, nullptr, &instance ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create instance!" );
	}
}

void vk::setupDebugCallback( ) {

	if( !enableValidationLayers ) return;

	VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
	createInfo.sType           = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
	createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
	createInfo.messageType     = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
	createInfo.pfnUserCallback = debugCallback;

	if( CreateDebugUtilsMessengerEXT( instance, &createInfo, nullptr, &callback ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to set up debug callback!" );
	}
}

void vk::createSurface( ) {

	if( glfwCreateWindowSurface( instance, mWindowHandle, nullptr, &surface ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create window surface!" );
	}
}

void vk::pickPhysicalDevice( ) {

	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices( instance, &deviceCount, nullptr );

	if( deviceCount == 0 ) {
		throw std::runtime_error( "failed to find GPUs with Vulkan support!" );
	}

	std::vector<VkPhysicalDevice> devices( deviceCount );
	vkEnumeratePhysicalDevices( instance, &deviceCount, devices.data( ) );

	for( const auto& device : devices ) {
		if( isDeviceSuitable( device ) ) {
			physicalDevice        = device;
			mSettings.msaaSamples = getMaxUsableSampleCount( VK_SAMPLE_COUNT_16_BIT ); // enable for MSAA VK_SAMPLE_COUNT_1_BIT; //
			break;
		}
	}

	if( physicalDevice == VK_NULL_HANDLE ) {
		throw std::runtime_error( "failed to find a suitable GPU!" );
	}
}

void vk::createLogicalDevice( ) {

	QueueFamilyIndices indices = findQueueFamilies( physicalDevice );

	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	std::set<int> uniqueQueueFamilies = { indices.graphicsFamily, indices.presentFamily };

	float queuePriority = 1.0f;
	for( int queueFamily : uniqueQueueFamilies ) {
		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType            = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount       = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back( queueCreateInfo );
	}

	VkPhysicalDeviceFeatures deviceFeatures = {};
	deviceFeatures.samplerAnisotropy        = VK_TRUE;
	deviceFeatures.fillModeNonSolid         = VK_TRUE;
	deviceFeatures.fragmentStoresAndAtomics = VK_TRUE;

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType                   = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.queueCreateInfoCount    = static_cast<uint32_t>( queueCreateInfos.size( ) );
	createInfo.pQueueCreateInfos       = queueCreateInfos.data( );
	createInfo.pEnabledFeatures        = &deviceFeatures;
	createInfo.enabledExtensionCount   = static_cast<uint32_t>( deviceExtensions.size( ) );
	createInfo.ppEnabledExtensionNames = deviceExtensions.data( );

	if( enableValidationLayers ) {
		createInfo.enabledLayerCount   = static_cast<uint32_t>( validationLayers.size( ) );
		createInfo.ppEnabledLayerNames = validationLayers.data( );
	} else {
		createInfo.enabledLayerCount = 0;
	}

	if( vkCreateDevice( physicalDevice, &createInfo, nullptr, &device ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create logical device!" );
	}

	vkGetDeviceQueue( device, indices.graphicsFamily, 0, &graphicsQueue );
	vkGetDeviceQueue( device, indices.presentFamily,  0, &presentQueue  );
}

void vk::createSwapChain( ) {

	SwapChainSupportDetails swapChainSupport = querySwapChainSupport( physicalDevice );
	VkSurfaceFormatKHR surfaceFormat         = chooseSwapSurfaceFormat( swapChainSupport.formats );
	VkPresentModeKHR presentMode             = chooseSwapPresentMode( swapChainSupport.presentModes );
	VkExtent2D extent                        = chooseSwapExtent( swapChainSupport.capabilities );

	uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
	if( swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount ) {
		imageCount = swapChainSupport.capabilities.maxImageCount;
	}

	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType            = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface          = surface;
	createInfo.minImageCount    = imageCount;
	createInfo.imageFormat      = surfaceFormat.format;
	createInfo.imageColorSpace  = surfaceFormat.colorSpace;
	createInfo.imageExtent      = extent;
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage       = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	QueueFamilyIndices indices = findQueueFamilies( physicalDevice );
	uint32_t queueFamilyIndices[] = { (uint32_t) indices.graphicsFamily, (uint32_t) indices.presentFamily };

	if( indices.graphicsFamily != indices.presentFamily ) {
		createInfo.imageSharingMode      = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices   = queueFamilyIndices;
	} else {
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	}

	createInfo.preTransform   = swapChainSupport.capabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.presentMode    = presentMode;
	createInfo.clipped        = VK_TRUE;

	if( vkCreateSwapchainKHR( device, &createInfo, nullptr, &swapChain ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create swap chain!" );
	}

	vkGetSwapchainImagesKHR( device, swapChain, &imageCount, nullptr );
	swapChainImages.resize( imageCount );
	vkGetSwapchainImagesKHR( device, swapChain, &imageCount, swapChainImages.data( ) );

	swapChainImageFormat = surfaceFormat.format;
	swapChainExtent      = extent;
}

void vk::createImageViews( ) {

	swapChainImageViews.resize( swapChainImages.size( ) );

	for( uint32_t i = 0; i < swapChainImages.size( ); i++ ) {
		swapChainImageViews[i] = createImageView( device, swapChainImages[i], swapChainImageFormat, VK_IMAGE_ASPECT_COLOR_BIT, 1 );
	}
}

void vk::createForwardRenderPass( ) {

	VkAttachmentReference colorAttachmentRef;
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout     = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkAttachmentReference depthAttachmentRef = {};
	depthAttachmentRef.attachment = 1;
	depthAttachmentRef.layout     = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkAttachmentReference colorAttachmentResolveRef = {};
	colorAttachmentResolveRef.attachment = 2;
	colorAttachmentResolveRef.layout     = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	std::vector<VkAttachmentDescription> attachments = {
		mBackBuffer.GetAttachmentDesc( ),
		mDepthBuffer.GetAttachmentDesc( ),
		mBackBuffer.GetResolveAttachmentDesc( )
	};

	mForwardRenderPass.CreateRenderPass( &colorAttachmentRef, 1,
										 &depthAttachmentRef,
										 &colorAttachmentResolveRef,
										 attachments, false, device );
}

void vk::createGeometryRenderPass( ) {
	
	// diffuse
	VkAttachmentReference colorAttachmentDiffuseRef = {};
	colorAttachmentDiffuseRef.attachment = 0;
	colorAttachmentDiffuseRef.layout     = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	// specular
	VkAttachmentReference colorAttachmentSpecularRef = {};
	colorAttachmentSpecularRef.attachment = 0;
	colorAttachmentSpecularRef.layout	  = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	// normal
	VkAttachmentReference colorAttachmentNormalRef = {};
	colorAttachmentNormalRef.attachment = 1;
	colorAttachmentNormalRef.layout     = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	// depth
	VkAttachmentReference depthAttachmentRef = {};
	depthAttachmentRef.attachment = 1;
	depthAttachmentRef.layout     = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	// diffuse resolve
	VkAttachmentReference colorAttachmentResolveRef = {};
	colorAttachmentResolveRef.attachment = 2;
	colorAttachmentResolveRef.layout     = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	// gbuffer attachments
	VkAttachmentReference __colourAttachments[] = {
		colorAttachmentDiffuseRef,
		colorAttachmentSpecularRef,
		colorAttachmentNormalRef
	};

	std::vector<VkAttachmentDescription> attachments;
	// diff/spec/normal
	for( auto& target : mGbuffer ) {
		attachments.push_back( target.GetAttachmentDesc( ) );
	}
	// depth
	attachments.push_back( mDepthBuffer.GetAttachmentDesc( ) );
	// diffuse (resolve)
	attachments.push_back( mGbuffer[0].GetResolveAttachmentDesc( ) );

	mGbufferRenderPass.CreateRenderPass( __colourAttachments, sizeof( VkAttachmentReference ) / sizeof( __colourAttachments ),
										 &depthAttachmentRef,
										 &colorAttachmentResolveRef,
										 attachments, false, device );
}

void vk::createLightRenderPass( ) {
}

void vk::createDescriptorSetLayouts( ) {

	uniformCameraMat.CreateDescriptorSetLayout( VK_SHADER_STAGE_VERTEX_BIT, 0 );
	uniformAppData.CreateDescriptorSetLayout( VK_SHADER_STAGE_FRAGMENT_BIT, 1 );
	storageMultiLightsData.CreateDescriptorSetLayout( VK_SHADER_STAGE_FRAGMENT_BIT, 2 );

	TextureImage texImg;
	texImg.CreateDescriptorSetLayout( VK_SHADER_STAGE_FRAGMENT_BIT, 3 );

	//mRenderList[0]->GetDiffuseTexture( 0 ).CreateDescriptorSetLayout( VK_SHADER_STAGE_FRAGMENT_BIT, 3 );
//	mRenderList[1]->GetDiffuseTexture( 0 ).CreateDescriptorSetLayout( VK_SHADER_STAGE_FRAGMENT_BIT, 3 );

	std::vector<VkDescriptorSetLayoutBinding> bindings = {
		uniformCameraMat.GetDescriptorLayout( ),
		uniformAppData.GetDescriptorLayout( ),
		storageMultiLightsData.GetDescriptorLayout( ),
		texImg.GetDescriptorLayout( )
	};

	VkDescriptorSetLayoutCreateInfo layoutInfo = {};
	layoutInfo.sType        = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = static_cast<uint32_t>( bindings.size( ) );
	layoutInfo.pBindings    = bindings.data( );

	if( vkCreateDescriptorSetLayout( device, &layoutInfo, nullptr, &descriptorSetLayout ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create descriptor set layout!" );
	}
}

void vk::createForwardGraphicsPipeline( ) {

	Shader vert, frag;

	vert.Create( device, mShaderDirectory, "transform.vert.spv" );
	frag.Create( device, mShaderDirectory, "ForwardLit.frag.spv" );

	mShaderStages.resize( 2 );
	mShaderStages[0] = SetVertex( vert.GetShaderModule( ), vert.GetEntryPoint( ) );
	mShaderStages[1] = SetFragment( frag.GetShaderModule( ), frag.GetEntryPoint( ) );

	mForwardPipeline.SetInputAssembler( Vertex::getBindingDescription( ), Vertex::getAttributeDescriptions( ),
									    VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, VK_FALSE );

	mForwardPipeline.SetViewport( swapChainExtent );
	mForwardPipeline.SetRasterizer( VK_TRUE, VK_TRUE, VK_COMPARE_OP_LESS, mSettings.msaaSamples, VK_FALSE );
	mForwardPipeline.SetShaderStages( mShaderStages );
	mForwardPipeline.SetPushConstants<glm::mat4x4>( 0, VK_SHADER_STAGE_VERTEX_BIT );

	mForwardPipeline.CreatePipelineLayout( device, &descriptorSetLayout, 1, 1 );
	mForwardPipeline.CreateGraphicsPipeline( device, mForwardRenderPass.GetRenderPass( ) );
}

void vk::createGbufferGraphicsPipeline( ) {

	Shader vert, frag;
}

void vk::createCullLightsGraphicsPipeline( ) {

	Shader vert, frag;
}

void vk::createSkyboxAndLightingGraphicsPipeline( ) {

	Shader vert, frag;
}

void vk::createForwardFramebuffers( ) {

	mSwapChainFramebuffers.resize( swapChainImageViews.size( ) );

	for( size_t i = 0; i < swapChainImageViews.size( ); i++ ) {
		std::vector<VkImageView> attachments = {
			mBackBuffer.GetImageView( ),
			mDepthBuffer.GetImageView( ),
			swapChainImageViews[i]
		};

		mSwapChainFramebuffers[i].Initialise( attachments, mForwardRenderPass.GetRenderPass( ),
											  swapChainExtent, 1, device );
	}
}

void vk::createGBufferFramebuffers( ) {
}

void vk::createCommandPool( ) {

	QueueFamilyIndices queueFamilyIndices = findQueueFamilies( physicalDevice );

	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType            = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;
	// important: without cannot re-use current command buffers for recording commands each frame
	poolInfo.flags            = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	if( vkCreateCommandPool( device, &poolInfo, nullptr, &commandPool ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create graphics command pool!" );
	}
}

void vk::createColorResources( ) {

	mBackBuffer.Create( physicalDevice, device, commandPool, graphicsQueue, swapChainImageFormat,
						swapChainExtent.width, swapChainExtent.height, 1, mSettings.msaaSamples,
						VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
						VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL );
	/*	GBuffer
		0: Diffuse
		1: Specular
		2: Normal
		3: Depth (read-only) (created in 'createDepthResources()')
	*/
	mGbuffer.resize( 3 );
	for( auto& buffer : mGbuffer ) {
		buffer.Create( physicalDevice, device, commandPool, graphicsQueue, swapChainImageFormat,
				  swapChainExtent.width, swapChainExtent.height, 1, mSettings.msaaSamples,
				  VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
				  VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL );
	}
}

void vk::createDepthResources( ) {

	mDepthBuffer.Create( physicalDevice, device, commandPool, graphicsQueue,
						 swapChainExtent.width, swapChainExtent.height, 1, mSettings.msaaSamples,
						 VK_IMAGE_TILING_OPTIMAL, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT );
}

VkSampleCountFlagBits vk::getMaxUsableSampleCount( VkSampleCountFlagBits _sampleAmount, const bool&& _disable ) {

	// (debug) will complain about 1 sample when using resolve attachment
	if( _disable ) {
		return VK_SAMPLE_COUNT_1_BIT;
	}

	VkPhysicalDeviceProperties physicalDeviceProperties;
	vkGetPhysicalDeviceProperties( physicalDevice, &physicalDeviceProperties );

	VkSampleCountFlags counts = std::min( physicalDeviceProperties.limits.framebufferColorSampleCounts, physicalDeviceProperties.limits.framebufferDepthSampleCounts );
	if( counts & VK_SAMPLE_COUNT_64_BIT && _sampleAmount == VK_SAMPLE_COUNT_64_BIT ) { return VK_SAMPLE_COUNT_64_BIT; }
	if( counts & VK_SAMPLE_COUNT_32_BIT && _sampleAmount == VK_SAMPLE_COUNT_32_BIT ) { return VK_SAMPLE_COUNT_32_BIT; }
	if( counts & VK_SAMPLE_COUNT_16_BIT && _sampleAmount == VK_SAMPLE_COUNT_16_BIT ) { return VK_SAMPLE_COUNT_16_BIT; }
	if( counts & VK_SAMPLE_COUNT_8_BIT	&& _sampleAmount == VK_SAMPLE_COUNT_8_BIT  ) { return VK_SAMPLE_COUNT_8_BIT;  }
	if( counts & VK_SAMPLE_COUNT_4_BIT	&& _sampleAmount == VK_SAMPLE_COUNT_4_BIT  ) { return VK_SAMPLE_COUNT_4_BIT;  }
	if( counts & VK_SAMPLE_COUNT_2_BIT	&& _sampleAmount == VK_SAMPLE_COUNT_2_BIT  ) { return VK_SAMPLE_COUNT_2_BIT;  }

	std::string msg = "your graphics device doesn't support selected multi-sampling count: 'VK_SAMPLE_COUNT_" + 
						std::to_string( _sampleAmount ) + "_BIT', so has defaulted to 'VK_SAMPLE_COUNT_1_BIT'";
	gLogger.Warning( msg, "getMaxUsableSampleCount()" );

	return VK_SAMPLE_COUNT_1_BIT;
}

void vk::createTextureImages( ) {

	for( auto& model : mRenderList ) {
		model->LoadTextures( physicalDevice, commandPool, graphicsQueue );
	}
}

void vk::createTextureSamplers( ) {

	linearAnisotropicSampler.Create( device, VK_FILTER_LINEAR,
									 VK_SAMPLER_ADDRESS_MODE_REPEAT, static_cast<float>( 1.0f ),
									 VK_TRUE, 16 );
}

void vk::createUniformBuffers( ) {

	uniformCameraMat.Initialise( physicalDevice, device, swapChainImages.size( ) );
	uniformAppData.Initialise( physicalDevice, device, swapChainImages.size( ) );

	for( size_t i = 0; i < swapChainImages.size( ); i++ ) {
		uniformCameraMat.AllocateBuffer( i );
		uniformAppData.AllocateBuffer( i );
	}
}

void vk::RecreateLightBuffer( ) {

	storageMultiLightsData.Destroy( );

	storageMultiLightsData.Initialise( physicalDevice, device, swapChainImages.size( ) );
	storageMultiLightsData.SetTotalInstances( mActiveLights.size( ) );

	for( size_t i = 0; i < swapChainImages.size( ); i++ ) {
		storageMultiLightsData.AllocateBuffer( i );
		storageMultiLightsData.CreateDescriptorSet( i, VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET, descriptorSets[i], 2 );
		storageMultiLightsData.UpdateDescriptorSet( );
	}
}

void vk::createStorageBuffers( ) {

	storageMultiLightsData.Initialise( physicalDevice, device, swapChainImages.size( ) );
	storageMultiLightsData.SetTotalInstances( mActiveLights.size( ) );

	for( size_t i = 0; i < swapChainImages.size( ); i++ ) {
		storageMultiLightsData.AllocateBuffer( i );
	}
}

void vk::createDescriptorPool( ) {

	std::vector<VkDescriptorPoolSize> poolSizes = {
		{ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, static_cast<uint32_t>( swapChainImages.size( ) ) },
		{ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, static_cast<uint32_t>( swapChainImages.size( ) ) },
		{ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, static_cast<uint32_t>( swapChainImages.size( ) ) },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,		 static_cast<uint32_t>( swapChainImages.size( ) ) },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,		 static_cast<uint32_t>( swapChainImages.size( ) ) },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,		 static_cast<uint32_t>( swapChainImages.size( ) ) },
		{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,		 static_cast<uint32_t>( swapChainImages.size( ) ) }
	};

	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType         = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = static_cast<uint32_t>( poolSizes.size( ) );
	poolInfo.pPoolSizes    = poolSizes.data( );
	poolInfo.maxSets       = static_cast<uint32_t>( swapChainImages.size( ) );

	if( vkCreateDescriptorPool( device, &poolInfo, nullptr, &descriptorPool ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create descriptor pool!" );
	}
}

void vk::createDescriptorSets( ) {

	std::vector<VkDescriptorSetLayout> layouts( swapChainImages.size( ), descriptorSetLayout );
	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType              = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool     = descriptorPool;
	allocInfo.descriptorSetCount = static_cast<uint32_t>( swapChainImages.size( ) );
	allocInfo.pSetLayouts        = layouts.data( );

	descriptorSets.resize( swapChainImages.size( ) );
	if( vkAllocateDescriptorSets( device, &allocInfo, &descriptorSets[0] ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to allocate descriptor sets!" );
	}	

	for( size_t i = 0; i < swapChainImages.size( ); i++ ) {

		for( auto &model : mRenderList ) {
			for( auto &tex : model->GetDiffuseTextures( ) ) {
				tex.CreateDescriptorSet( i, VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET, descriptorSets[i], 3, linearAnisotropicSampler.GetSampler( ) );
			}
		}

		uniformCameraMat.CreateDescriptorSet( i, VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET, descriptorSets[i], 0 );
		uniformAppData.CreateDescriptorSet( i, VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET, descriptorSets[i], 1 );
		storageMultiLightsData.CreateDescriptorSet( i, VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET, descriptorSets[i], 2 );

		uniformCameraMat.UpdateDescriptorSet( );
		uniformAppData.UpdateDescriptorSet( );
		storageMultiLightsData.UpdateDescriptorSet( );

		for( auto &model : mRenderList ) {
			for( auto &tex : model->GetDiffuseTextures( ) ) {
				tex.UpdateDescriptorSet( );
			}
		}
	}
}

void vk::createSyncObjects( ) {

	imageAvailableSemaphores.resize( MAX_FRAMES_IN_FLIGHT );
	renderFinishedSemaphores.resize( MAX_FRAMES_IN_FLIGHT );
	inFlightFences.resize( MAX_FRAMES_IN_FLIGHT );

	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fenceInfo = {};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for( size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++ ) {
		if( vkCreateSemaphore( device, &semaphoreInfo, nullptr, &imageAvailableSemaphores[i] ) != VK_SUCCESS ||
			vkCreateSemaphore( device, &semaphoreInfo, nullptr, &renderFinishedSemaphores[i] ) != VK_SUCCESS ||
			vkCreateFence( device, &fenceInfo, nullptr, &inFlightFences[i] ) != VK_SUCCESS ) {
			throw std::runtime_error( "failed to create synchronization objects for a frame!" );
		}
	}
}

void vk::updateDynamicUniformBuffer( uint32_t currentImage ) {

}

void vk::updateUniformBuffer( uint32_t currentImage ) {

	static auto startTime = std::chrono::high_resolution_clock::now( );

	auto currentTime = std::chrono::high_resolution_clock::now( );
	float time = std::chrono::duration<float, std::chrono::seconds::period>( currentTime - startTime ).count( );

	uniformCameraMat.GetInstance( ).view  = mViewer->GetViewMatrix( );
	uniformCameraMat.GetInstance( ).proj  = mViewer->GetProjMatrix( );
	uniformCameraMat.Map( currentImage );
	
	uniformAppData.GetInstance( ).CameraPosition = glm::vec4( 1.0f );
	uniformAppData.Map( currentImage );
}

void vk::updateStorageBuffer( uint32_t currentImage ) {

	int i = 0;
	for( auto& light : mActiveLights ) {
		storageMultiLightsData.GetInstance( i ).pointLight = *light.get( );
		i++;
	}
	storageMultiLightsData.Map( );
}

template<typename T>
inline void vk::updatePushConstants( const T& __mappedData, const VkCommandBuffer _cmdBuffer, const VkPipelineLayout _pipelineLayout, const VkShaderStageFlagBits && _shaderStageFlag ) {

	vkCmdPushConstants( _cmdBuffer, _pipelineLayout, _shaderStageFlag, 0, sizeof( T ), &__mappedData );
}

template<typename T>
void vk::updatePushConstants( const T& __mappedData, const VkPipelineLayout _pipelineLayout, const VkShaderStageFlagBits && _shaderStageFlag ) {

	VkCommandBuffer tmpCommandBuffer = beginSingleTimeCommands( device, commandPool );

	vkCmdPushConstants( tmpCommandBuffer, _pipelineLayout, _shaderStageFlag, 0, sizeof( T ), &__mappedData );

	endSingleTimeCommands( device, commandPool, tmpCommandBuffer, graphicsQueue );
}

VkSurfaceFormatKHR vk::chooseSwapSurfaceFormat( const std::vector<VkSurfaceFormatKHR>& availableFormats ) {
	
	if( availableFormats.size( ) == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED ) {
		return{ VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
	}

	for( const auto& availableFormat : availableFormats ) {
		if( availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR ) {
			return availableFormat;
		}
	}

	return availableFormats[0];
}

VkPresentModeKHR vk::chooseSwapPresentMode( const std::vector<VkPresentModeKHR> availablePresentModes ) {

	VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;

	for( const auto& availablePresentMode : availablePresentModes ) {
		if( availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR ) {
			return availablePresentMode;
		} else if( availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR ) {
			bestMode = availablePresentMode;
		}
	}

	return bestMode;
}

VkExtent2D vk::chooseSwapExtent( const VkSurfaceCapabilitiesKHR & capabilities ) {
	
	if( capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max( ) ) {
		return capabilities.currentExtent;
	} else {
		int width, height;
		glfwGetFramebufferSize( mWindowHandle, &width, &height );

		VkExtent2D actualExtent = {
			static_cast<uint32_t>( width ),
			static_cast<uint32_t>( height )
		};

		actualExtent.width  = std::max( capabilities.minImageExtent.width, std::min( capabilities.maxImageExtent.width, actualExtent.width ) );
		actualExtent.height = std::max( capabilities.minImageExtent.height, std::min( capabilities.maxImageExtent.height, actualExtent.height ) );

		return actualExtent;
	}
}

SwapChainSupportDetails vk::querySwapChainSupport( VkPhysicalDevice device ) {
	
	SwapChainSupportDetails details;

	vkGetPhysicalDeviceSurfaceCapabilitiesKHR( device, surface, &details.capabilities );

	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR( device, surface, &formatCount, nullptr );

	if( formatCount != 0 ) {
		details.formats.resize( formatCount );
		vkGetPhysicalDeviceSurfaceFormatsKHR( device, surface, &formatCount, details.formats.data( ) );
	}

	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR( device, surface, &presentModeCount, nullptr );

	if( presentModeCount != 0 ) {
		details.presentModes.resize( presentModeCount );
		vkGetPhysicalDeviceSurfacePresentModesKHR( device, surface, &presentModeCount, details.presentModes.data( ) );
	}

	return details;
}

bool vk::isDeviceSuitable( VkPhysicalDevice device ) {

	QueueFamilyIndices indices = findQueueFamilies( device );

	bool extensionsSupported = checkDeviceExtensionSupport( device );

	bool swapChainAdequate = false;
	if( extensionsSupported ) {
		SwapChainSupportDetails swapChainSupport = querySwapChainSupport( device );
		swapChainAdequate = !swapChainSupport.formats.empty( ) && !swapChainSupport.presentModes.empty( );
	}

	VkPhysicalDeviceFeatures supportedFeatures;
	vkGetPhysicalDeviceFeatures( device, &supportedFeatures );

	return indices.isComplete( ) && extensionsSupported && swapChainAdequate  && supportedFeatures.samplerAnisotropy;
}

bool vk::checkDeviceExtensionSupport( VkPhysicalDevice device ) {

	uint32_t extensionCount;
	vkEnumerateDeviceExtensionProperties( device, nullptr, &extensionCount, nullptr );

	std::vector<VkExtensionProperties> availableExtensions( extensionCount );
	vkEnumerateDeviceExtensionProperties( device, nullptr, &extensionCount, availableExtensions.data( ) );

	std::set<std::string> requiredExtensions( deviceExtensions.begin( ), deviceExtensions.end( ) );

	for( const auto& extension : availableExtensions ) {
		requiredExtensions.erase( extension.extensionName );
	}

	return requiredExtensions.empty( );
}

QueueFamilyIndices vk::findQueueFamilies( VkPhysicalDevice device ) {
	
	QueueFamilyIndices indices;

	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties( device, &queueFamilyCount, nullptr );

	std::vector<VkQueueFamilyProperties> queueFamilies( queueFamilyCount );
	vkGetPhysicalDeviceQueueFamilyProperties( device, &queueFamilyCount, queueFamilies.data( ) );

	int i = 0;
	for( const auto& queueFamily : queueFamilies ) {
		if( queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT ) {
			indices.graphicsFamily = i;
		}

		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR( device, i, surface, &presentSupport );

		if( queueFamily.queueCount > 0 && presentSupport ) {
			indices.presentFamily = i;
		}

		if( indices.isComplete( ) ) {
			break;
		}

		i++;
	}

	return indices;
}

std::vector<const char*> vk::getRequiredExtensions( ) {
	
	uint32_t glfwExtensionCount = 0;
	const char** glfwExtensions;
	glfwExtensions = glfwGetRequiredInstanceExtensions( &glfwExtensionCount );

	std::vector<const char*> extensions( glfwExtensions, glfwExtensions + glfwExtensionCount );

	if( enableValidationLayers ) {
		extensions.push_back( VK_EXT_DEBUG_UTILS_EXTENSION_NAME );
	}

	return extensions;
}

bool vk::checkValidationLayerSupport( ) {

	uint32_t layerCount;
	vkEnumerateInstanceLayerProperties( &layerCount, nullptr );

	std::vector<VkLayerProperties> availableLayers( layerCount );
	vkEnumerateInstanceLayerProperties( &layerCount, availableLayers.data( ) );

	for( const char* layerName : validationLayers ) {
		bool layerFound = false;

		for( const auto& layerProperties : availableLayers ) {
			if( strcmp( layerName, layerProperties.layerName ) == 0 ) {
				layerFound = true;
				break;
			}
		}

		if( !layerFound ) {
			return false;
		}
	}

	return true;
}